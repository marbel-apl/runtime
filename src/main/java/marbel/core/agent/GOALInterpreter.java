/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.core.agent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import marbel.core.executors.modules.ModuleExecutor;
import marbel.core.executors.stack.CallStack;
import marbel.core.executors.stack.StackExecutor;
import marbel.core.runtime.service.agent.NettoRunTime;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.actions.MentalAction;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.preferences.CorePreferences;
import marbel.preferences.ProfilerPreferences;
import marbel.tools.debugger.Debugger;
import marbel.tools.debugger.DebuggerKilledException;
import marbel.tools.debugger.ObservableDebugger;
import marbel.tools.debugger.SteppingDebugger;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventListener;
import marbel.tools.errorhandling.Warning;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;
import marbel.tools.errorhandling.exceptions.GOALLaunchFailureException;
import marbel.tools.errorhandling.exceptions.GOALRuntimeErrorException;
import marbel.tools.logging.InfoLog;
import marbel.tools.profiler.Profiler;
import marbel.tools.profiler.Profiles;

/**
 * Agent controller (interpreter) for an agent ({@link AgentDefinition}).
 * <p>
 * The controller can be provided with a {@link Debugger} that will be called at
 * specific points during a run.
 * </p>
 *
 * A {@link Learner} will be consulted during the Adaptive sections of a
 * AgentDefinition.
 *
 * @param <DEBUGGER> class of the Debugger used by the interpreter.
 */
public class GOALInterpreter<DEBUGGER extends Debugger> extends Controller {
	/**
	 * The {@link RunState} of this agent. Records the current state of the
	 * interpreter in the GOAL Program.
	 */
	private RunState runState;
	/**
	 * Program ran by the interpreter.
	 */
	private final AgentDefinition agentDf;
	/**
	 * The agent registry.
	 */
	private final AgentRegistry<?> registry;
	/**
	 * Debugger used while running the interpreter.
	 */
	private final DEBUGGER debugger;
	/**
	 * The executor call stack
	 */
	private final CallStack stack;
	/**
	 * The profiler. null if disabled
	 */
	private Profiler profiler = null;
	/**
	 * The timeout. 0 if disabled
	 */
	private long timeout = 0;
	private final Profiles profiles;

	/**
	 * Constructs a new interpreter.
	 *
	 * @param agentDf  to run
	 * @param debugger used to debug the program
	 * @param learner  used to evaluate adaptive modules
	 * @param profiles the profiles database of all agents
	 */
	public GOALInterpreter(final AgentDefinition agentDf, final AgentRegistry<?> registry, final DEBUGGER debugger,
			final Profiles profiles) {
		this.agentDf = agentDf;
		this.registry = registry;
		this.debugger = debugger;
		this.profiles = profiles;
		this.stack = new CallStack();
	}

	/**
	 * @return the current run state of the interpreter
	 */
	public RunState getRunState() {
		return this.runState;
	}

	/*
	 * @return the current debugger used by the interpeter
	 */
	public DEBUGGER getDebugger() {
		return this.debugger;
	}

	@Override
	protected void initalizeController(final Agent<? extends Controller> agent, final ExecutorService executor,
			final long timeout) throws GOALLaunchFailureException {
		super.initalizeController(agent, executor, timeout);
		this.timeout = timeout;
	}

	protected void createRunstate() throws GOALLaunchFailureException {
		this.runState = new RunState(this, this.agent.getId(), this.agent.getEnvironment(), this.agent.getLogging(),
				this.agentDf, this.registry, this.timeout);

		final ExecutionEventListener listener = new ExecutionEventListener() {
			@Override
			public void goalEvent(final DebugChannel channel, final Object associateObject,
					final SourceInfo associateSource, final String message, final Object... args) {
				GOALInterpreter.this.debugger.breakpoint(channel, associateObject, associateSource, message, args);
			}
		};
		this.runState.getEventGenerator().addListener(listener);

		if (ProfilerPreferences.getProfiling()) {
			this.profiler = new Profiler(this.runState.getTimer(), this.agentDf.getName());
			this.runState.getEventGenerator().addListener(this.profiler);
		}
	}

	@Override
	public void onReset() {
		try {
			this.debugger.reset();
			this.runState.reset();
		} catch (final GOALLaunchFailureException e) {
			throw new GOALRuntimeErrorException(e); // FIXME
		}
	}

	@Override
	public void onTerminate() {
		final Module shutdown = (this.runState == null) ? null : this.runState.getShutdownModule();
		if (shutdown != null) {
			try {
				final CallStack temp = new CallStack();
				final ModuleExecutor shutdownExec = ModuleExecutor.getModuleExecutor(temp, this.runState, shutdown,
						shutdown.getDefaultSubstitution(), RuleEvaluationOrder.LINEARALL);
				temp.push(shutdownExec);
				this.debugger.reset();
				while (temp.canExecute()) {
					temp.pop();
					temp.getPopped().getResult();
				}
			} catch (final GOALActionFailedException e) {
				new Warning("failed to execute shutdown module", e).emit();
			}
		}
		this.debugger.kill();

		// show the profile results if enabled.
		if (this.profiler != null) {
			this.profiler.stop();
			this.profiles.add(this.profiler.getProfile());
			this.profiler.getProfile().log(this.agent.getId());
		}
	}

	@Override
	protected Runnable getRunnable(final ExecutorService executor, final Callable<Callable<?>> in) {
		return new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				try {
					Callable<Callable<?>> call = in;
					if (call == null) {
						// Inform the timer that thread is started, and create
						// the runstate
						final NettoRunTime localtimer = new NettoRunTime();
						createRunstate();
						if (GOALInterpreter.this.debugger instanceof ObservableDebugger) {
							GOALInterpreter.this.runState.getEventGenerator()
									.event(DebugChannel.REASONING_CYCLE_SEPARATOR, 0, null, "starting agent.");
						} else {
							new InfoLog("starting agent '" + GOALInterpreter.this.agent.getId() + "'.").emit();
						}
						// Add the main module to the execution stack.
						final Module main = GOALInterpreter.this.runState.getMainModule();
						final ModuleExecutor mainExec = ModuleExecutor.getModuleExecutor(GOALInterpreter.this.stack,
								GOALInterpreter.this.runState, main, main.getDefaultSubstitution(),
								RuleEvaluationOrder.LINEAR);
						// mainExec.setSource(UseCase.MAIN);
						GOALInterpreter.this.stack.push(mainExec);
						mainExec.loadKR(); // required before receiving messages/percepts!

						// Start the first cycle.
						final boolean eventOccurred = GOALInterpreter.this.runState.startCycle(false);

						// If we have an event module, AND there are some events
						// to proces, add it to the stack as well,
						// i.e. execute it before the main module.
						final Module event = GOALInterpreter.this.runState.getEventModule();
						if (event != null && eventOccurred) {
							final ModuleExecutor eventExec = ModuleExecutor.getModuleExecutor(
									GOALInterpreter.this.stack, GOALInterpreter.this.runState, event,
									event.getDefaultSubstitution(), RuleEvaluationOrder.LINEARALL);
							// eventExec.setSource(UseCase.EVENT);
							GOALInterpreter.this.stack.push(eventExec);
						}
						// If we have an init module, add it to the stack as
						// well, i.e. execute it once before the other modules.
						final Module init = GOALInterpreter.this.runState.getInitModule();
						if (init != null) {
							final ModuleExecutor initExec = ModuleExecutor.getModuleExecutor(GOALInterpreter.this.stack,
									GOALInterpreter.this.runState, init, init.getDefaultSubstitution(),
									RuleEvaluationOrder.LINEARALL);
							// initExec.setSource(UseCase.INIT);
							GOALInterpreter.this.stack.push(initExec);
						}
						// Make executing the first cycle the initial call.
						call = () -> executeCycle(GOALInterpreter.this.runState.getRoundCounter());
						// Report that we are ready now
						final long took = (localtimer.get() / 1000000);
						if (GOALInterpreter.this.debugger instanceof ObservableDebugger) {
							GOALInterpreter.this.runState.getEventGenerator().event(
									DebugChannel.REASONING_CYCLE_SEPARATOR, 0, null,
									"started agent (took " + took + "ms).");
						} else {
							new InfoLog(
									"started agent '" + GOALInterpreter.this.agent.getId() + "' (took " + took + "ms).")
									.emit();
						}
					}
					Callable<Callable<?>> out = null;
					if (call != null) {
						// Run the current task
						out = (Callable<Callable<?>>) call.call();
					}
					if (out != null && isRunning()) {
						// Submit the next task (when any)
						executor.execute(getRunnable(executor, out));
					} else {
						// Clean-up (terminate/dispose)
						setTerminated();
						new InfoLog("agent '" + GOALInterpreter.this.agent.getId() + "' terminated successfully.")
								.emit();
					}
				} catch (final Exception e) { // Thread failure handling
					GOALInterpreter.this.throwable = e;
					if (e instanceof DebuggerKilledException) {
						// "normal" forced termination by the debugger.
						new InfoLog("agent '" + GOALInterpreter.this.agent.getId() + "' was killed externally.", e)
								.emit();
					} else {
						// something went wrong
						new Warning("agent '" + GOALInterpreter.this.agent.getId() + "' was forcefully terminated.", e)
								.emit();
					}
					try {
						setTerminated();
					} catch (final InterruptedException ie) {
						new Warning("unable to properly terminate agent '" + GOALInterpreter.this.agent.getId() + "'.",
								ie).emit();
					}
				}
			}
		};
	}

	/**
	 * Uses the current {@link CallStack} for execution, i.e. popping (and thus
	 * executing) {@link StackExecutor}s from it, but only as long as we are in the
	 * indicated cycle. If we're not, and the stack is not empty (yet), a
	 * {@link Callable} will be returned that can be used to execute the next cycle
	 * (i.e. by feeding it into the executorservice).
	 *
	 * @param cycle the cycle to execute
	 * @return
	 * @throws GOALActionFailedException
	 */
	private Callable<Callable<?>> executeCycle(final int cycle) throws GOALActionFailedException {
		while (CorePreferences.getSequentialExecution() && isRunning() && !hasTurn()) {
			// in sequential mode, wait until we are given the turn
			try {
				Thread.sleep(1);
			} catch (final InterruptedException e) {
				break;
			}
		}
		try {
			while (isRunning() && this.stack.canExecute() && this.runState.getRoundCounter() == cycle) {
				this.stack.pop();
				this.stack.getPopped().getResult();
			}
		} finally {
			endTurn();
			this.runState.getTimer().leaveThread();
		}

		if (isRunning() && this.stack.canExecute()) {
			return () -> executeCycle(cycle + 1);
		} else {
			return null;
		}
	}

	/**
	 * @see {@link CallStack#getIndex()}
	 */
	public int getStackIndex() {
		return this.stack.getIndex();
	}

	/**
	 * @return the program ran by the interpreter
	 */
	public AgentDefinition getProgram() {
		return this.agentDf;
	}

	/**
	 * Execute actions manually.
	 *
	 * @param actions The actions to be executed.
	 * @throws GOALActionFailedException
	 */
	public void doPerformAction(final ActionCombo actions) throws GOALActionFailedException {
		for (final Action<?> action : actions.getActions()) {
			final boolean keeprunning = (action instanceof MentalAction) && (this.debugger instanceof SteppingDebugger);
			if (keeprunning) {
				((SteppingDebugger) this.debugger).setKeepRunning(true);
			}
			this.runState.doPerformAction(action);
			if (keeprunning) {
				((SteppingDebugger) this.debugger).setKeepRunning(false);
			}
		}
	}

	@Override
	public void dispose() {
		this.debugger.dispose();

		if (this.runState != null) {
			this.runState.dispose();
		}

		this.stack.clear();

		// agent is disposed by the AgentService, which is in turn disposed by
		// the RuntimeManager, so we don't need to do that here
	}
}
