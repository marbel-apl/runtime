package marbel.core.executors.stack;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

import marbel.core.runtime.mentalstate.MentalStateConditionExecutor;
import marbel.core.runtime.mentalstate.MentalStateConditionResult;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.agent.rules.ForallDoRule;
import marbel.languageTools.program.agent.rules.Rule;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Executor for a {@link Rule}. Keeps its own stack of
 * {@link ActionComboStackExecutor}s as determined by {@link #pushed()}, which
 * evaluates the rule's condition. Not all possible actioncombo instantiations
 * that result from the initialization will be pushed to the {@link CallStack}
 * (and thus executed), for example when executing an if-then rule.
 */
public class RuleStackExecutor extends StackExecutor {
	/**
	 * The rule to be evaluated and, if applicable, to be applied.
	 */
	private final Rule rule;
	/**
	 * Substitution to be used for instantiating variables in the rule.
	 */
	private final Substitution substitution;
	/**
	 * The module that we're executing the rule in. CHECK can we remove this? We now
	 * only need it to get KR Language
	 */
	private Module context;
	/**
	 * The actions to execute as initially determined by {@link #pushed()}
	 */
	private Deque<ActionComboStackExecutor> actions;
	/**
	 * The last result of an execute-call
	 */
	private AgentActionResult result;
	/**
	 * A possible exception (instead of a result)
	 */
	private GOALActionFailedException failure;

	/**
	 * Create an executor for a {@link Rule}.
	 *
	 * @param parent       The {@link CallStack} that we are working in.
	 * @param runstate     The {@link RunState} (i.e. agent) that we are working
	 *                     for.
	 * @param rule         The {@link Rule} that is to be evaluated and, if
	 *                     applicable, applied.
	 * @param substitution The {@link Substitution} that is to be used for
	 *                     instantiating variables in the rule.
	 */
	public RuleStackExecutor(final CallStack parent, final RunState runstate, final Rule rule,
			final Substitution substitution) {
		super(parent, runstate);
		this.rule = rule;
		this.substitution = substitution;
	}

	/**
	 * @param context The {@link Module} that we are executing the rule in.
	 */
	public void setContext(final Module context) {
		this.context = context;
	}

	@Override
	public void popped() {
		if (this.failure != null) {
			return;
		}

		final ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();
		if (this.actions == null) {
			generator.event(DebugChannel.RULE_START, this.rule, this.rule.getCondition().getSourceInfo(),
					"evaluating '%s'.", this.rule);

			this.result = AgentActionResult.START;
			try {
				this.actions = generateExecutors();
			} catch (final GOALActionFailedException e) {
				this.failure = e;
			}
		}

		if (this.failure != null) {
			return;
		}

		try {
			// If we have just finished executing an actioncombo, we use its result as our
			// result. This assumes the strict order of elements on the {@link CallStack}.
			final ActionComboStackExecutor previous = (getPrevious() instanceof ActionComboStackExecutor)
					? (ActionComboStackExecutor) getPrevious()
					: null;
			if (previous != null) {
				this.result = this.result.merge(previous.getResult());
			}
			// Check if we need to stop executing the rule.
			final boolean all = (this.rule instanceof ForallDoRule);
			final boolean exit = (this.actions.isEmpty()) || (!all && this.result.justPerformedAction());
			if (exit) {
				this.actions.clear();
				generator.event(DebugChannel.RULE_EXIT, this.rule, this.rule.getCondition().getSourceInfo(),
						"finished rule '%s'.", this.rule);
			} else {
				// Put the rule itself back on the stack,
				// and add the next action to execute to it.
				select(this);
				select(this.actions.remove());
			}
		} catch (final GOALActionFailedException e) {
			this.failure = e;
		}

	}

	@Override
	public AgentActionResult getResult() throws GOALActionFailedException {
		if (this.failure == null) {
			return this.result;
		} else {
			throw this.failure;
		}
	}

	/**
	 * Generates action executors from evaluation results. Only generates executors
	 * for actions whose precondition holds.
	 *
	 * @return A list of action executors.
	 *
	 * @throws GOALActionFailedException If the evaluation of the rule condition
	 *                                   failed.
	 */
	public Deque<ActionComboStackExecutor> generateExecutors() throws GOALActionFailedException {
		final List<ActionComboStackExecutor> executors = new ArrayList<>();
		try {
			final MentalStateConditionResult result = evaluateRule();
			if (result.holds()) {
				for (final Substitution substitution : result.getAnswers()) {
					final ActionComboStackExecutor executor = (ActionComboStackExecutor) getExecutor(
							this.rule.getAction(), substitution);
					executors.add(executor);
				}
				// use the runtime rule order of the module we're in
				final RuleEvaluationOrder order = (this.parent.getParentModuleExecutor() == null)
						? this.context.getRuleEvaluationOrder()
						: this.parent.getParentModuleExecutor().getRuleOrder();
				if (order == RuleEvaluationOrder.LINEARRANDOM || order == RuleEvaluationOrder.LINEARALLRANDOM
						|| order == RuleEvaluationOrder.RANDOM || order == RuleEvaluationOrder.RANDOMALL) {
					Collections.shuffle(executors);
				}
			}
			return new ArrayDeque<>(executors);
		} catch (final KRQueryFailedException e) {
			throw new GOALActionFailedException(
					"failed to evaluate condition of '" + this.rule.applySubst(this.substitution) + "'", e);
		} finally {
			this.runstate.getEventGenerator().event(DebugChannel.RULE_EVAL_CONDITION_DONE, this.rule,
					this.rule.getCondition().getSourceInfo(), "evaluated '%s'", this.rule);
		}
	}

	/**
	 * @return the results of evaluating the rule's {@link MentalStateCondition}
	 * @throws KRQueryFailedException
	 */
	private MentalStateConditionResult evaluateRule() throws KRQueryFailedException {
		final MentalStateWithEvents mst = this.runstate.getMentalState();
		final ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();
		final Rule instantiatedRule = this.rule.applySubst(this.substitution);

		generator.event(DebugChannel.RULE_CONDITIONAL_VIEW, instantiatedRule.getCondition(),
				instantiatedRule.getCondition().getSourceInfo(), "evaluating condition of '%s'.", instantiatedRule);
		final MentalStateConditionExecutor msce = mst.getConditionExecutor(this.rule.getCondition(), this.substitution);
		final MentalStateConditionResult mscresult = msce.evaluate(mst, this.runstate.getEventGenerator());
		// Report on results and generate the list of actioncombo
		// executors for this rule (if the condition applies).
		if (mscresult.holds()) {
			generator.event(DebugChannel.HIDDEN_RULE_CONDITION_EVALUATION, instantiatedRule.getAction(),
					instantiatedRule.getCondition().getSourceInfo(), "condition of '%s' holds.", instantiatedRule);
			generator.event(DebugChannel.RULE_CONDITION_EVALUATION, instantiatedRule.getCondition(),
					instantiatedRule.getCondition().getSourceInfo(), "condition of '%s' holds for: %s.",
					instantiatedRule, mscresult.getAnswers());
		} else {
			generator.event(DebugChannel.RULE_CONDITION_EVALUATION, instantiatedRule.getCondition(),
					instantiatedRule.getCondition().getSourceInfo(), "condition of '%s' does not hold.",
					instantiatedRule);
		}
		return mscresult;
	}

	@Override
	public String toString() {
		return "RuleStackExecutor for " + this.rule.getCondition() + " with " + this.substitution;
	}
}