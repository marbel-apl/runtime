package marbel.core.executors;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKr.TranslationException;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.core.agent.AgentRegistry;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Term;
import marbel.languageTools.program.agent.AgentId;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.Warning;

/**
 * Extends selector from mental state project with a method to evaluate a
 * selector using an agent registry.
 */
public class SelectorExecutor {
	private final Term selector;

	public SelectorExecutor(final Term selector) {
		this.selector = selector;
	}

	public List<AgentId> evaluate(final RunState state) throws InstantiationFailedException, TranslationException {
		final AgentId aid = state.getId();
		final AgentRegistry<?> registry = state.getRegistry();
		final ExecutionEventGeneratorInterface events = state.getEventGenerator();
		final CognitiveKR translator = CognitiveKRFactory.getCognitiveKR(state.getKRI());
		final List<Term> terms = translator.unpackTerm(this.selector);

		final List<AgentId> agents = new ArrayList<>();
		for (final Term term : terms) {
			String name = term.toString();
			switch (name) {
			case "self":
				agents.add(aid);
				break;
			case "all": // ALL is inclusive; keep own aid.
				agents.addAll(registry.getRegisteredAgents());
				break;
			case "allother": // ALLOTHER is exclusive; remove own aid.
				final List<AgentId> registered1 = new ArrayList<>(registry.getRegisteredAgents());
				registered1.remove(aid);
				if (registered1.isEmpty()) {
					events.event(DebugChannel.WARNING, new Warning("no other agent(s) to send messages to."),
							term.getSourceInfo());
				} else {
					agents.addAll(registered1);
				}
				break;
			case "some": // SOME is inclusive; keep own aid.
				final List<AgentId> registered2 = new ArrayList<>(registry.getRegisteredAgents());
				final int pick1 = new Random().nextInt(registered2.size());
				agents.add(registered2.get(pick1));
				break;
			case "someother": // SOMEOTHER is exclusive; remove own aid.
				final List<AgentId> registered3 = new ArrayList<>(registry.getRegisteredAgents());
				registered3.remove(aid);
				if (registered3.isEmpty()) {
					events.event(DebugChannel.WARNING, new Warning("no other agent to send messages to."),
							term.getSourceInfo());
				} else {
					final int pick2 = new Random().nextInt(registered3.size());
					agents.add(registered3.get(pick2));
				}
				break;
			default:
				// Strip single or double quotes in term's name, if any.
				if (name.startsWith("\'") || name.startsWith("\"")) {
					name = name.substring(1, name.length() - 1);
				}
				// Try to find the referenced agent or channel...
				if (registry.isChannel(name)) {
					agents.addAll(registry.getSubscribers(name));
				} else {
					agents.add(new AgentId(name));
				}
				break;
			}
		}

		return agents;
	}
}