package marbel.core.executors.modules;

import java.util.List;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKr.TranslationException;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.core.executors.stack.CallStack;
import marbel.core.executors.stack.StackExecutor;
import marbel.core.runtime.mentalstate.MentalStateResult;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.core.runtime.service.agent.AgentActionResult.RunStatus;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Abstract class for module executors, and also factory for module executors.
 *
 * <p>
 * An action executor determines how to execute a {@link Module}.
 * <p>
 */
public abstract class ModuleExecutor extends StackExecutor {
	/**
	 * The module to be executed.
	 */
	private final Module module;
	/**
	 * Substitution to be used for instantiating parameters of the module.
	 */
	private final Substitution substitution;
	/**
	 * Have we prepared the mental state for the module yet? A call to
	 * {@link #prepareMentalState()} will put this to true.
	 */
	private boolean loadedKr, prepared;
	/**
	 * the order in which the rules in this module are to be evaluated if the module
	 * didn't specify an order
	 */
	private RuleEvaluationOrder ruleOrder;
	/**
	 * The last result of an execute-call.
	 */
	protected AgentActionResult result;
	/**
	 * A possible exception (instead of a result)
	 */
	protected GOALActionFailedException failure;

	/**
	 * Create an executor for a {@link Module}.
	 *
	 * @param parent       The {@link CallStack} that we are working in.
	 * @param runstate     The {@link RunState} (i.e. agent) that we are working
	 *                     for.
	 * @param module       The {@link Module} that is to be executed.
	 * @param substitution The {@link Substitution} to be used for instantiating
	 *                     parameters of the module.
	 * @param ruleOrder    the default rule order, to be used if the module does not
	 *                     specify a rule order. Must never be null.
	 */
	ModuleExecutor(final CallStack parent, final RunState runstate, final Module module,
			final Substitution substitution, final RuleEvaluationOrder defaultRuleOrder) {
		super(parent, runstate);
		this.module = module;
		this.substitution = substitution;
		this.ruleOrder = module.getRuleEvaluationOrder();
		if (this.ruleOrder == null) {
			this.ruleOrder = defaultRuleOrder;
		}
	}

	/**
	 * @return The module to be executed.
	 */
	public Module getModule() {
		return this.module;
	}

	/**
	 * @return The substitution that ...
	 */
	public Substitution getSubstitution() {
		return this.substitution;
	}

	/**
	 * @return True iff prepareMentalState has been executed successfully
	 */
	protected boolean hasPreparedMentalState() {
		return this.prepared;
	}

	/**
	 * If the module has a focus option, then create a new attention set in line
	 * with the option specified. Then add beliefs and goals from module's beliefs
	 * and goals section to the agent's mental state.
	 *
	 * @param runState The run state used to prepare the module's execution.
	 * @throws GOALActionFailedException If inserting a belief or adopting a marbel
	 *                                   failed.
	 */
	protected void prepareMentalState() throws GOALActionFailedException {
		// Push (non-anonymous) modules that were just entered onto
		// stack that keeps track of modules that have been entered but
		// not yet exited again and initialize the mental state for the
		// module, i.e. initial beliefs/goals and possible focus.
		if (!this.module.isAnonymous()) {
			this.runstate.getEventGenerator().event(DebugChannel.MODULE_ENTRY, this.module, this.module.getDefinition(),
					"entered '%s' with %s.", this.module, getSubstitution());
		}
		this.runstate.enterModule(this.module);

		if (!this.loadedKr) {
			loadKR();
		}

		this.prepared = true;
	}

	public void loadKR() throws GOALActionFailedException {
		final List<DatabaseFormula> kr = this.module.getKR();
		if (!kr.isEmpty()) {
			try {
				final MentalStateWithEvents mentalState = this.runstate.getMentalState();
				final ExecutionEventGeneratorInterface generator = this.runstate.getEventGenerator();
				final CognitiveKR translator = CognitiveKRFactory.getCognitiveKR(this.runstate.getKRI());
				final MentalStateResult inserted = mentalState.insert(translator.makeUpdate(kr), generator);
				generator.event(DebugChannel.KR_UPDATES, inserted, this.module.getDefinition());
				this.loadedKr = true;
			} catch (KRDatabaseException | InstantiationFailedException | TranslationException e) {
				throw new GOALActionFailedException("execution of module '" + this.module + "' failed.", e);
			}
		}
	}

	/**
	 * Check whether we need to start a new cycle. We do so if we do NOT exit this
	 * module and are running within the main module's context (never start a new
	 * cycle when running the init/event or a module called from either of these
	 * modules).
	 *
	 * @return true if the event module executor has been pushed onto the call
	 *         stack. If this happens, the caller should probably return too, to let
	 *         the handling of the stack proceed.
	 */
	protected boolean doEvent(final boolean reset, final AgentActionResult previousResult)
			throws GOALActionFailedException {
		if (reset && this.runstate.isMainModuleRunning()) {
			this.runstate.startCycle(previousResult.justPerformedRealAction());
			final Module event = this.runstate.getEventModule();
			if (event != null) {
				final ModuleExecutor exec = ModuleExecutor.getModuleExecutor(this.parent, this.runstate, event,
						event.getDefaultSubstitution(), RuleEvaluationOrder.LINEARALL);
				select(this);
				select(exec);
				return true;
			}
		}
		return false;
	}

	/**
	 * Evaluates whether the module should be terminated.
	 *
	 * @param runState The run state used for evaluating the termination conditions.
	 * @return {@code true} if the module needs to be terminated; {@code false}
	 *         otherwise.
	 * @throws GOALActionFailedException
	 */
	protected boolean isModuleTerminated(final boolean noMoreRules) throws GOALActionFailedException {
		// Set exit flag if {@link ExitModuleAction} has been performed.
		boolean exit = this.result.getStatus() != RunStatus.RUNNING;
		// Evaluate module's exit condition.
		if (noMoreRules) {
			switch (getModule().getExitCondition()) {
			case NOACTION:
				exit |= !this.result.justPerformedAction();
				break;
			case ALWAYS:
				exit |= true;
				break;
			default:
			case NEVER:
				// exit whenever module has been terminated (see above)
				break;
			}
		}
		exit |= this.runstate.getParent().isTerminated();
		return exit;
	}

	/**
	 * Takes all the necessary actions to properly exit a module.
	 */
	protected void terminateModule() throws GOALActionFailedException {
		// Remove the module from the tack of modules that have been
		// entered and possibly update top level context in which we run
		this.runstate.exitModule(this.module);
		// Report the module exit on the module's debug channel.
		if (!this.module.isAnonymous()) {
			this.result = this.result.merge(AgentActionResult.SOFTSTOP);
			this.runstate.getEventGenerator().event(DebugChannel.MODULE_EXIT, this.module, this.module.getDefinition(),
					"exited '%s'.", this.module);
		}
	}

	/**
	 * @return the {@link RuleEvaluationOrder} of this module executor.
	 */
	public RuleEvaluationOrder getRuleOrder() {
		return this.ruleOrder;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " for " + getModule() + " with " + getSubstitution();
	}

	/**
	 * ModuleExecutor factory. Creates a module executor for the module.
	 *
	 * @param parent           The {@link CallStack} that we are working in.
	 * @param runstate         The {@link RunState} (i.e. agent) that we are working
	 *                         for.
	 * @param module           The {@link Module} to create an executor for.
	 * @param substitution     The {@link Substitution} to be used for instantiating
	 *                         parameters of the module.
	 * @param defaultRuleOrder the order in which the rules in this module are to be
	 *                         evaluated if the module didn't specify an order.
	 * @return A module executor for the module. If the module requests specific
	 *         rule order, that order is used; otherwise the default rule order is
	 *         used.
	 */
	public static ModuleExecutor getModuleExecutor(final CallStack parent, final RunState runstate, final Module module,
			final Substitution substitution, final RuleEvaluationOrder defaultRuleOrder) {
		RuleEvaluationOrder order = module.getRuleEvaluationOrder();
		if (order == null) {
			order = defaultRuleOrder;
		}
		return new LinearModuleExecutor(parent, runstate, module, substitution, order);

	}
}
