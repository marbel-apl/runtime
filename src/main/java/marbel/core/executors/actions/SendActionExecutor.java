/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.core.executors.actions;

import java.util.List;

import marbel.cognitiveKr.TranslationException;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.core.executors.SelectorExecutor;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.languageTools.program.agent.AgentId;
import marbel.languageTools.program.agent.actions.SendAction;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Executor for the send action.
 */
public class SendActionExecutor extends ActionExecutor {
	/**
	 * Executor for the send action.
	 *
	 * @param action       A send action.
	 * @param substitution Substitution for instantiating parameters of the send
	 *                     action.
	 */
	SendActionExecutor(final SendAction action, final Substitution substitution) {
		super(action, substitution);
	}

	@Override
	public AgentActionResult execute(final RunState runState) throws GOALActionFailedException {
		final SendAction send = (SendAction) getAction();

		try {
			final Term message = send.getUpdate().applySubst(getSubstitution());
			final Term target = send.getTarget().applySubst(getSubstitution());
			final List<AgentId> receivers = new SelectorExecutor(target).evaluate(runState);

			runState.send(send, message, receivers);

			return new AgentActionResult(getAction());
		} catch (final InstantiationFailedException | TranslationException e) {
			throw new GOALActionFailedException("Unable to determine message receivers.", e);
		}
	}
}