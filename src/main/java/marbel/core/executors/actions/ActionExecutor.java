/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.core.executors.actions;

import marbel.core.executors.stack.ActionStackExecutor;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.CancelTimerAction;
import marbel.languageTools.program.agent.actions.DeleteAction;
import marbel.languageTools.program.agent.actions.ExitModuleAction;
import marbel.languageTools.program.agent.actions.InsertAction;
import marbel.languageTools.program.agent.actions.LogAction;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.languageTools.program.agent.actions.PrintAction;
import marbel.languageTools.program.agent.actions.SendAction;
import marbel.languageTools.program.agent.actions.SleepAction;
import marbel.languageTools.program.agent.actions.StartTimerAction;
import marbel.languageTools.program.agent.actions.SubscribeAction;
import marbel.languageTools.program.agent.actions.UnsubscribeAction;
import marbel.languageTools.program.agent.actions.UserSpecAction;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;
import marbel.tools.errorhandling.exceptions.GOALRuntimeErrorException;

/**
 * Abstract class for action executors, and also factory for action executors.
 *
 * <p>
 * An action executor determines how to execute an {@link Action}: apply the
 * current substitution, evaluate its precondition, executing an environment
 * action, changing the mental state, etc.
 * <p>
 * Usually {@link ActionExecutor}s are not executed directly but inserted into
 * an {@link ActionStackExecutor}.
 */
public abstract class ActionExecutor {
	/**
	 * The action to be executed.
	 */
	private final Action<?> action;
	/**
	 * Substitution to be used for instantiating parameters of action.
	 */
	private final Substitution substitution;

	/**
	 * Executor for action.
	 *
	 * @param action       The {@link Action} to be executed.
	 *
	 * @param substitution Substitution for instantiating parameters of the
	 *                     user-specified action. These are the parameter values as
	 *                     on the caller's side. e.g. <code>[X/1, Y/2]</code>. There
	 *                     may be additional variables in this substi that are not
	 *                     used in the actual action.
	 */
	ActionExecutor(final Action<?> action, final Substitution substitution) {
		this.action = action;
		this.substitution = substitution;
	}

	/**
	 * @return The action to be executed.
	 */
	public Action<?> getAction() {
		return this.action;
	}

	/**
	 * @return The substitution that has been passed on for instantiating action
	 *         parameters. These are the parameter values as on the caller's side.
	 *         e.g. <code>[X/1, Y/2]</code>. There may be additional variables in
	 *         this substi that are not used in the actual action.
	 */
	public Substitution getSubstitution() {
		return this.substitution;
	}

	/**
	 * Default implementation for test whether action can be executed.
	 *
	 * @return {@code true} if action is closed; {@false otherwise}.
	 */
	public boolean canBeExecuted() {
		return getAction().applySubst(getSubstitution()).isClosed();
	}

	/**
	 * Implements the action specific execution method.
	 *
	 * @param runState The current run state.
	 * @return The result of performing the action.
	 * @throws GOALActionFailedException If executing the action failed.
	 */
	public abstract AgentActionResult execute(RunState runState) throws GOALActionFailedException;

	/**
	 * Performs the action using {@link #execute(RunState)}. First evaluates the
	 * precondition, generates the resulting action executors, and then executes
	 * them.
	 *
	 * @param runState The run state in which the action is executed.
	 * @return The result of performing the action.
	 * @throws GOALActionFailedException If the action is not closed.
	 */
	public final AgentActionResult perform(final RunState runState) throws GOALActionFailedException {
		if (canBeExecuted()) {
			final AgentActionResult result = execute(runState);
			DebugChannel report = DebugChannel.ACTION_EXECUTED_BUILTIN;
			if (getAction() instanceof UserSpecAction) {
				report = DebugChannel.ACTION_EXECUTED_USERSPEC;
			} else if (getAction() instanceof SendAction) {
				report = DebugChannel.ACTION_EXECUTED_MESSAGING;
			}
			runState.getEventGenerator().event(report, getAction(), getAction().getSourceInfo(), "performed '%s'.",
					getAction().applySubst(getSubstitution()));
			return result;
		} else {
			throw new GOALActionFailedException("attempt to execute '" + getAction() + "' with " + getSubstitution()
					+ " left free variables: " + getAction().applySubst(getSubstitution()).getFreeVar() + ".");
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " for " + getAction().getName() + " with " + getSubstitution();
	}

	/**
	 * ActionExecutor factory. Creates an action executor for the action.
	 *
	 * @param action       The action for which an executor is created.
	 * @param substitution Substitution for instantiating the action's parameters.
	 * @return An action executor for the action.
	 */
	public static ActionExecutor getActionExecutor(final Action<?> action, final Substitution substitution) {
		if (action instanceof DeleteAction) {
			return new DeleteActionExecutor((DeleteAction) action, substitution);
		} else if (action instanceof ExitModuleAction) {
			return new ExitModuleActionExecutor((ExitModuleAction) action, substitution);
		} else if (action instanceof InsertAction) {
			return new InsertActionExecutor((InsertAction) action, substitution);
		} else if (action instanceof LogAction) {
			return new LogActionExecutor((LogAction) action, substitution);
		} else if (action instanceof ModuleCallAction) {
			return new ModuleCallActionExecutor((ModuleCallAction) action, substitution);
		} else if (action instanceof PrintAction) {
			return new PrintActionExecutor((PrintAction) action, substitution);
		} else if (action instanceof SendAction) {
			return new SendActionExecutor((SendAction) action, substitution);
		} else if (action instanceof SleepAction) {
			return new SleepActionExecutor((SleepAction) action, substitution);
		} else if (action instanceof SubscribeAction) {
			return new SubscribeActionExecutor((SubscribeAction) action, substitution);
		} else if (action instanceof UnsubscribeAction) {
			return new UnsubscribeActionExecutor((UnsubscribeAction) action, substitution);
		} else if (action instanceof UserSpecAction) {
			return new UserSpecActionExecutor((UserSpecAction) action, substitution);
		} else if (action instanceof StartTimerAction) {
			return new StartTimerActionExecutor((StartTimerAction) action, substitution);
		} else if (action instanceof CancelTimerAction) {
			return new CancelTimerActionExecutor((CancelTimerAction) action, substitution);
		} else {
			throw new GOALRuntimeErrorException("unknown action type '" + action.getClass() + "'.");
		}
	}
}
