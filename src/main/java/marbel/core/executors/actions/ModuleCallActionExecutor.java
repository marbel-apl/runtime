/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.core.executors.actions;

import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Executor for the module call action.
 */
public class ModuleCallActionExecutor extends ActionExecutor {
	/**
	 * Executor for the module call action.
	 *
	 * @param action       A module call action.
	 * @param substitution Substitution for instantiating parameters of the module
	 *                     call action.
	 */
	ModuleCallActionExecutor(final ModuleCallAction action, final Substitution substitution) {
		super(action, substitution);
	}

	/**
	 * A module call action which does not have a named module as target, i.e. is
	 * associated with a set of nested rules, can always be executed. Otherwise, the
	 * parameters of the action should be closed.
	 *
	 * @return {@code true} if targeted module is either anonymous, or the
	 *         parameters of the module call are closed.
	 */
	@Override
	public boolean canBeExecuted() {
		if (((ModuleCallAction) getAction()).getTarget().isAnonymous()) {
			return true;
		} else {
			return super.canBeExecuted();
		}
	}

	@Override
	public AgentActionResult execute(final RunState runState) throws GOALActionFailedException {
		throw new RuntimeException("a ModuleCallAction cannot be executed independently.");
	}

}