/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.core.executors.actions;

import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.PrintAction;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Executor for the print action.
 */
public class PrintActionExecutor extends ActionExecutor {
	/**
	 * Executor for the print action.
	 *
	 * @param action       A print action.
	 * @param substitution Substitution for instantiating parameters of the print
	 *                     action.
	 */
	PrintActionExecutor(final PrintAction action, final Substitution substitution) {
		super(action, substitution);
	}

	@Override
	public AgentActionResult execute(final RunState runState) throws GOALActionFailedException {
		final Action<?> instantiated = getAction().applySubst(getSubstitution());
		runState.doPerformAction(instantiated);
		return new AgentActionResult(instantiated);
	}
}
