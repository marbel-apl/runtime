/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.core.executors.actions;

import marbel.core.runtime.mentalstate.MentalStateResult;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Update;
import marbel.languageTools.program.agent.actions.DeleteAction;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Executor for the delete action.
 */
public class DeleteActionExecutor extends ActionExecutor {
	/**
	 * Executor for the delete action.
	 *
	 * @param action       A delete action.
	 * @param substitution Substitution for instantiating parameters of the delete
	 *                     action.
	 */
	DeleteActionExecutor(final DeleteAction action, final Substitution substitution) {
		super(action, substitution);
	}

	@Override
	public AgentActionResult execute(final RunState runState) throws GOALActionFailedException {
		final DeleteAction delete = (DeleteAction) getAction();
		final Update update = delete.getUpdate().applySubst(getSubstitution());

		final MentalStateWithEvents mentalState = runState.getMentalState();
		final ExecutionEventGeneratorInterface generator = runState.getEventGenerator();
		try {
			final MentalStateResult removed = mentalState.delete(update, generator);
			generator.event(DebugChannel.KR_UPDATES, removed, delete.getSourceInfo());
			return new AgentActionResult(getAction());
		} catch (final KRDatabaseException e) {
			throw new GOALActionFailedException("failed to execute '" + delete + "' with " + getSubstitution() + ".",
					e);
		}
	}
}
