package marbel.core.runtime.mentalstate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import marbel.krInterface.language.DatabaseFormula;

/**
 * Result contains the changes on a database after it was modified: which
 * formulas were added and deleted, what is the new focus.
 *
 */
public class MentalStateResult {
	private final List<DatabaseFormula> added;
	private final List<DatabaseFormula> removed;

	public MentalStateResult() {
		this.added = new ArrayList<>();
		this.removed = new ArrayList<>();
	}

	public void added(final List<DatabaseFormula> dbfs) {
		this.added.addAll(dbfs);
	}

	public void added(final DatabaseFormula dbf) {
		this.added.add(dbf);
	}

	public void removed(final List<DatabaseFormula> dbfs) {
		this.removed.addAll(dbfs);
	}

	public void removed(final DatabaseFormula dbf) {
		this.removed.add(dbf);
	}

	public List<DatabaseFormula> getAdded() {
		return this.added;
	}

	public List<DatabaseFormula> getRemoved() {
		return this.removed;
	}

	public void merge(final MentalStateResult other) {
		added(other.added);
		removed(other.removed);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		if (!this.added.isEmpty()) {
			builder.append("inserted ").append(listToString(this.added));
		}
		if (!this.removed.isEmpty()) {
			if (builder.length() > 0) {
				builder.append("and ");
			}
			builder.append("deleted ").append(listToString(this.removed));
		}
		if (builder.length() < 1) {
			builder.append("empty update");
		}
		builder.append(".");
		return builder.toString();
	}

	protected char[] listToString(final List<DatabaseFormula> formulas) {
		// replace [..] with '..'
		final char[] removedlist = formulas.toString().toCharArray();
		removedlist[0] = '\'';
		removedlist[removedlist.length - 1] = removedlist[0];
		return removedlist;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.added, this.removed);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof MentalStateResult)) {
			return false;
		}
		final MentalStateResult other = (MentalStateResult) obj;
		if (!Objects.equals(this.added, other.added) || !Objects.equals(this.removed, other.removed)) {
			return false;
		}
		return true;
	}
}
