/**
 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.core.runtime.mentalstate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import marbel.krInterface.language.Substitution;

/**
 * Container for results obtained by evaluating a mental state condition.
 * <p>
 * The evaluation of a mental state condition can give two types of results:
 * <ul>
 * <li>A (possibly empty) set of answers, i.e., substitutions that instantiate
 * the condition such that it holds in a mental state, and</li>
 * <li>A (possibly empty) mapping of substitutions to single goals, where each
 * substitution when applied yields an instantiation of the condition that holds
 * in a mental state where the marbel base only contains the associated marbel.</li>
 * </ul>
 * </p>
 */
public class MentalStateConditionResult {
	/**
	 * Mapping of goals to sets of substitutions.
	 */
	private final List<Substitution> results = new ArrayList<>();

	/**
	 * Creates a container for the results of a mental state condition evaluation.
	 */
	public MentalStateConditionResult() {

	}

	/**
	 * Sets the answers obtained by evaluating the mental state condition.
	 *
	 * @param answers The answers obtained by evaluating the mental state condition.
	 */
	protected void setAnswers(final Substitution substitution, final List<Substitution> answers) {
		for (final Substitution answer : answers) {
			this.results.add(substitution.combine(answer));
		}
	}

	/**
	 * Get the answers obtained by evaluating the mental state condition.
	 *
	 * return The answers obtained by evaluating the mental state condition.
	 */
	public List<Substitution> getAnswers() {
		return Collections.unmodifiableList(this.results);
	}

	/**
	 * @return {@code true} if the mental state condition holds, i.e., there is at
	 *         least one answer; {@code false} otherwise.
	 */
	public boolean holds() {
		return !this.results.isEmpty();
	}

	/**
	 * @return The number of answers obtained by evaluating the condition.
	 */
	public int nrOfAnswers() {
		return this.results.size();
	}
}