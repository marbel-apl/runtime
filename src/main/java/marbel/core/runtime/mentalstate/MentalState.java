/**
 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.core.runtime.mentalstate;

import java.util.List;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.krInterface.KRInterface;
import marbel.krInterface.database.Database;
import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Update;
import marbel.languageTools.program.agent.AgentId;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;

/**
 * Represents a mental state of an agent and provides query and update
 * functionality. A mental state consists of one or more {@link MentalModel}s: A
 * model to represent the percepts, messages, knowledge, beliefs, and goals of
 * the agent owner of this state and models for representing other agent's
 * beliefs and goals.
 * <p>
 * Assumes that a mental state is represented using a single knowledge
 * representation language, i.e. {@link KRInterface}.
 * </p>
 * <p>
 * To get notified about changes you can subscribe as Observer to the individual
 * {@link MentalBase}s. You can also subscribe to a top-level {@link GoalBase}
 * and receive all sub-module change info too.
 * </p>
 */
public class MentalState {
	/**
	 * The generic agent definition
	 */
	protected final AgentDefinition owner;
	/**
	 * The specific agent that owns this state.
	 */
	protected final AgentId agent;
	/**
	 * The database used for storing the contents of this base.
	 */
	protected final Database database;
	protected final CognitiveKR translator;

	/**
	 * Creates a mental state for an agent.
	 *
	 * @param owner The generic agent definition.
	 * @param agent The specific agent that owns this state.
	 */
	public MentalState(final AgentDefinition owner, final AgentId agent)
			throws InstantiationFailedException, KRDatabaseException {
		this.owner = owner;
		this.agent = agent;

		final KRInterface kri = owner.getKRInterface();
		this.translator = CognitiveKRFactory.getCognitiveKR(kri);
		this.database = kri.getDatabase(agent.toString());
	}

	/**
	 * @return The generic agent definition
	 */
	public AgentDefinition getOwner() {
		return this.owner;
	}

	/**
	 * @return The specific agent that owns this state.
	 */
	public AgentId getAgent() {
		return this.agent;
	}

	/**
	 * @return The set of formulas that are in the belief base of the agent that
	 *         owns this mental state or in the mental model that this agent has of
	 *         another agent.
	 */
	public List<DatabaseFormula> getFormulas() throws KRDatabaseException {
		return this.database.getFormulas();
	}

	public int getFormulaCount() throws KRDatabaseException {
		return this.database.getFormulaCount();
	}

	/**
	 *
	 *
	 * @param condition    The mental state condition to be evaluated.
	 * @param substitution Substitution for instantiating (free) variables in the
	 *                     mental state condition.
	 * @return An executor for evaluating a mental state condition.
	 */
	public MentalStateConditionExecutor getConditionExecutor(final MentalLiteral condition,
			final Substitution substitution) {
		return new MentalStateConditionExecutor(condition, substitution);
	}

	/**
	 * Evaluates the mental state condition on this mental state.
	 *
	 * @param msc          The mental state condition to evaluate.
	 * @param substitution The substitution to impose on the condition.
	 * @param generator    the channel to report executed actions to .
	 * @return MSCResult The result of the evaluation.
	 * @throws KRQueryFailedException
	 */
	public MentalStateConditionResult evaluate(final MentalLiteral msc, final Substitution substitution,
			final ExecutionEventGeneratorInterface generator) throws KRQueryFailedException {
		final List<Substitution> answers = query(msc.applySubst(substitution), generator);

		final MentalStateConditionResult evaluationResult = new MentalStateConditionResult();
		evaluationResult.setAnswers(substitution, answers);
		return evaluationResult;
	}

	/**
	 * Evaluate a {@link MentalLiteral} on this state. Such a literal can include
	 * selectors and involves a query on a certain base.
	 *
	 * @param literal   The literal to evaluate.
	 * @param generator the channel to report executed actions to .
	 * @return A set of substitutions, each of which make the evaluation true (or an
	 *         empty set otherwise). When the literal is a negative one, an empty
	 *         substitution set is returned if there was a result (i.e. there was a
	 *         solution), and a set containing one empty substitution is returned to
	 *         indicate a positive result (i.e. there was no solution).
	 * @throws KRQueryFailedException
	 */
	protected List<Substitution> query(final MentalLiteral literal, final ExecutionEventGeneratorInterface generator)
			throws KRQueryFailedException {
		return query(literal.getFormula());
	}

	@Override
	public String toString() {
		try {
			return this.owner.getName() + ":" + this.agent + ":" + getFormulas().toString();
		} catch (final KRDatabaseException e) {
			return e.getMessage();
		}
	}

	//
	// FORMER MENTALBASE
	//

	/**
	 * Dispose all resources.
	 *
	 * @throws KRDatabaseException
	 */
	public void destroy() throws KRDatabaseException {
		this.database.destroy();
	}

	/**
	 * Performs a query on the base and returns a non-empty set of substitutions if
	 * the query succeeds.
	 *
	 * @param formula The query.
	 * @return a set of substitutions each of which make the query true, or an empty
	 *         set otherwise.
	 * @throws KRQueryFailedException if query fails.
	 */
	public List<Substitution> query(final Query formula) throws KRQueryFailedException {
		return this.database.query(formula);
	}

	/*********** updating (insertion, deletion) methods ****************/

	/**
	 * Adds a {@link DatbaseFormula} to the base.
	 *
	 * @param formula The formula to be inserted (added).
	 * @throws KRDatabaseException
	 */
	public MentalStateResult insert(final DatabaseFormula formula) throws KRDatabaseException {
		final MentalStateResult result = new MentalStateResult();
		if (this.database.insert(formula)) {
			result.added(formula);
		}
		return result;
	}

	/**
	 * See {@link #update(List, List)}.
	 *
	 * @param update The update to be processed.
	 * @throws MSTQueryException
	 */
	public MentalStateResult insert(final Update update) throws KRDatabaseException {
		return update(update.getAddList(), update.getDeleteList());
	}

	/**
	 * Removes a {@link DatbaseFormula} from the base.
	 *
	 * @param formula The formula to be deleted (removed). The debugger monitoring
	 *                the removal.
	 * @throws KRDatabaseException
	 */
	public MentalStateResult delete(final DatabaseFormula formula) throws KRDatabaseException {
		final MentalStateResult result = new MentalStateResult();
		if (this.database.delete(formula)) {
			result.removed(formula);
		}
		return result;
	}

	/**
	 * See {@link #update(List, List)}. Deleting an {@link Update} is calling update
	 * with the add and delete lists reversed.
	 *
	 * @param update The update to be processed.
	 * @throws MSTQueryException
	 */
	public MentalStateResult delete(final Update update) throws KRDatabaseException {
		return update(update.getDeleteList(), update.getAddList());
	}

	/**
	 * Deletes the formulas in the delete list and adds the formulas in the add list
	 * to this base. First removes and then adds, so any formulas that appear in
	 * both lists will result in adding the formula if it is not already present.
	 *
	 * @param addList    The 'add' list of formulas that are to be inserted.
	 * @param deleteList The 'delete' list of formulas that are to be removed.
	 * @throws KRDatabaseExceptionp
	 */
	private MentalStateResult update(final List<DatabaseFormula> addList, final List<DatabaseFormula> deleteList)
			throws KRDatabaseException {
		final MentalStateResult result = new MentalStateResult();
		// First handle the delete list and remove formulas.
		for (final DatabaseFormula formula : deleteList) {
			result.merge(delete(formula));
		}
		// And then handle the add list and add formulas.
		for (final DatabaseFormula formula : addList) {
			result.merge(insert(formula));
		}
		return result;
	}
}
