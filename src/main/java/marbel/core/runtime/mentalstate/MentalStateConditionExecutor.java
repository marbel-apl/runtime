/**
 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.core.runtime.mentalstate;

import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;

/**
 * Executor for evaluating a mental state condition.
 */
public class MentalStateConditionExecutor {

	/**
	 * The mental state condition to be evaluated.
	 */
	private final MentalLiteral condition;
	/**
	 * Substitution for instantiating (free) variables in the mental state
	 * condition.
	 */
	private final Substitution substitution;

	/**
	 * Executor for evaluating a mental state condition.
	 *
	 * @param condition    The mental state condition to be evaluated.
	 * @param substitution Substitution for instantiating (free) variables in the
	 *                     mental state condition.
	 */
	public MentalStateConditionExecutor(final MentalLiteral condition, final Substitution substitution) {
		this.condition = condition;
		this.substitution = substitution;
	}

	/**
	 * Evaluates a mental state condition using a focus method.
	 *
	 * @param mentalState The mental state to evaluate the condition on.
	 * @param focus       the focus method.
	 * @param generator   the channel to report executed actions to .
	 * @return The {@link MentalStateConditionExecutor.MentalStateConditionResult}
	 *         obtained by evaluating the condition.
	 * @throws KRQueryFailedException If the evaluation failed.
	 */
	public MentalStateConditionResult evaluate(final MentalStateWithEvents mentalState,
			final ExecutionEventGeneratorInterface generator) throws KRQueryFailedException {
		return mentalState.evaluate(this.condition, this.substitution, generator);
	}

}