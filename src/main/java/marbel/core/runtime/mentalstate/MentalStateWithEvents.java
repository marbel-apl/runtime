/**
 * The GOAL Mental State. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.core.runtime.mentalstate;

import java.util.List;

import marbel.krInterface.exceptions.KRDatabaseException;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Update;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;

/**
 * wraps {@link MentalState}, generating events with the
 * {@link ExecutionEventGeneratorInterface}.
 */
public class MentalStateWithEvents {
	private final MentalState mentalState;
	private int queryCount = 0;

	/**
	 * wraps around given {@link MentalState}.
	 *
	 * @param ms the {@link MentalState} to wrap
	 */
	public MentalStateWithEvents(final MentalState ms) {
		this.mentalState = ms;
	}

	/**
	 * @return The agent that owns this mental state, i.e. the agent that actually
	 *         runs queries/updates on it.
	 */
	public AgentDefinition getOwner() {
		return this.mentalState.getOwner();
	}

	/**
	 * @return Unmodifiable set of formulas that are in the belief base of the agent
	 * @throws KRDatabaseException
	 */
	public List<DatabaseFormula> getFormulas() throws KRDatabaseException {
		return this.mentalState.getFormulas();
	}

	public int getFormulaCount() throws KRDatabaseException {
		return this.mentalState.getFormulaCount();
	}

	/**
	 *
	 *
	 * @param condition    The mental state condition to be evaluated.
	 * @param substitution Substitution for instantiating (free) variables in the
	 *                     mental state condition.
	 * @return An executor for evaluating a mental state condition.
	 */
	public MentalStateConditionExecutor getConditionExecutor(final MentalLiteral condition,
			final Substitution substitution) {
		return new MentalStateConditionExecutor(condition, substitution);
	}

	/**
	 * Evaluates the mental state condition on this mental state.
	 *
	 * @param msc          The mental state condition to evaluate.
	 * @param substitution The substitution to impose on the condition.
	 * @param generator    the channel to report executed actions to .
	 * @return MSCResult The result of the evaluation.
	 * @throws KRQueryFailedException
	 */
	public MentalStateConditionResult evaluate(final MentalLiteral msc, final Substitution substitution,
			final ExecutionEventGeneratorInterface generator) throws KRQueryFailedException {
		generator.event(DebugChannel.MSQUERY_START, msc, msc.getSourceInfo(), "evaluating mentalstate '%s'.", msc);

		final MentalStateConditionResult result = this.mentalState.evaluate(msc, substitution, generator);
		++this.queryCount;

		generator.event(DebugChannel.MSQUERY_END, msc, msc.getSourceInfo(), "evaluated mentalstate '%s'.", msc);

		return result;
	}

	public int getAndResetQueryCount() {
		final int count = this.queryCount;
		this.queryCount = 0;
		return count;
	}

	/**
	 * The update is inserted into the belief base of the agent.
	 *
	 * @param update    The update to be inserted.
	 * @param generator The channel to report executed actions to.
	 * @throws KRDatabaseException
	 */
	public MentalStateResult insert(final Update update, final ExecutionEventGeneratorInterface generator)
			throws KRDatabaseException {
		generator.event(DebugChannel.INSERT_START, update, update.getSourceInfo(), "insert %s", update);

		final MentalStateResult result = this.mentalState.insert(update);

		generator.event(DebugChannel.INSERT_END, update, update.getSourceInfo(), "inserted %s", update);

		return result;
	}

	/**
	 * The update is removed from the beliefbase of the agent.
	 *
	 * @param update    The belief to be removed.
	 * @param generator the channel to report executed actions to.
	 * @throws KRDatabaseException
	 */
	public MentalStateResult delete(final Update update, final ExecutionEventGeneratorInterface generator)
			throws KRDatabaseException {
		generator.event(DebugChannel.DELETE_START, update, update.getSourceInfo(), "delete %s", update);

		final MentalStateResult result = this.mentalState.delete(update);

		generator.event(DebugChannel.DELETE_END, update, update.getSourceInfo(), "deleted %s", update);

		return result;
	}

	public void destroy() throws KRDatabaseException {
		this.mentalState.destroy();
	}

	@Override
	public String toString() {
		return this.mentalState.toString();
	}
}
