/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test.executors;

import marbel.core.executors.stack.ActionComboStackExecutor;
import marbel.core.executors.stack.CallStack;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.KRInterface;
import marbel.languageTools.program.agent.Module.ExitCondition;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.languageTools.program.test.TestAction;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;
import marbel.tools.test.exceptions.TestActionFailed;
import marbel.tools.test.executors.testcondition.UntilExecutor;

public class TestActionExecutor extends TestObserver {
	private final TestAction action;

	public TestActionExecutor(final TestAction action) {
		this.action = action;
	}

	public TestAction getAction() {
		return this.action;
	}

	public void run(final RunState runstate) throws TestActionFailed {
		final KRInterface kri = runstate.getKRI();
		initialize(runstate);

		if (this.action.getCondition() != null) {
			add(new UntilExecutor(this.action.getCondition(), kri.getSubstitution(null), runstate, this));
		}

		// 'hack' for executing main modules manually
		for (final Action<?> action : this.action.getAction()) {
			if (action instanceof ModuleCallAction) {
				final ModuleCallAction moduleCall = (ModuleCallAction) action;
				final String module = moduleCall.getTarget().getSignature();
				if (runstate.getMainModule() != null && runstate.getMainModule().getSignature().equals(module)) {
					moduleCall.getTarget().setExitCondition(ExitCondition.NEVER);
				} else if ((runstate.getInitModule() != null && runstate.getInitModule().getSignature().equals(module))
						|| (runstate.getEventModule() != null
								&& runstate.getEventModule().getSignature().equals(module))) {
					moduleCall.getTarget().setRuleEvaluationOrder(RuleEvaluationOrder.LINEARALL);
				}
			}
		}

		final CallStack stack = new CallStack();
		final ActionComboStackExecutor actionExecutor = new ActionComboStackExecutor(stack, runstate,
				this.action.getAction(), kri.getSubstitution(null));
		stack.push(actionExecutor);

		try {
			while (stack.canExecute()) {
				stack.pop();
				stack.getPopped().getResult();
			}
		} catch (final GOALActionFailedException e) {
			throw new TestActionFailed(this, e);
		}
	}
}
