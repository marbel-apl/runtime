/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test.executors;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import marbel.core.agent.Agent;
import marbel.core.executors.modules.ModuleExecutor;
import marbel.core.executors.stack.CallStack;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.test.AgentTest;
import marbel.languageTools.program.test.ModuleTest;
import marbel.languageTools.program.test.TestAction;
import marbel.languageTools.program.test.TestProgram;
import marbel.tools.debugger.DebuggerKilledException;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;
import marbel.tools.test.TestInterpreter;
import marbel.tools.test.exceptions.BoundaryException;
import marbel.tools.test.exceptions.ConditionFailed;
import marbel.tools.test.exceptions.EvaluationFailed;
import marbel.tools.test.exceptions.TestActionFailed;
import marbel.tools.test.executors.testcondition.TestConditionExecutor;
import marbel.tools.test.result.AgentTestResult;
import marbel.tools.test.result.ModuleTestResult;
import marbel.tools.test.result.TestConditionResult;
import marbel.tools.test.result.TestResult;
import marbel.tools.test.result.TestResultFormatter;

public class AgentTestExecutor extends TestObserver {
	private final AgentTest test;
	private final TestProgram parent;
	private RunState runstate;
	private Map<String, ModuleTestExecutor> testMap;
	private Map<ModuleTestExecutor, TestResult> tests;

	public AgentTestExecutor(final AgentTest test, final TestProgram parent) {
		this.test = test;
		this.parent = parent;
	}

	public AgentTest getTest() {
		return this.test;
	}

	/**
	 * Runs this test on the given run state.
	 *
	 * @param agent the agent executing the test
	 * @return a list of {@link AssertTestResult}s containing the results of the
	 *         tests.
	 */
	public AgentTestResult run(final Agent<TestInterpreter> agent) throws GOALActionFailedException {
		this.testMap = new LinkedHashMap<>();
		this.tests = new LinkedHashMap<>();
		this.runstate = agent.getController().getRunState();

		final CallStack stack = new CallStack();
		initialize(this.runstate);
		final Module main = this.runstate.getMainModule();
		final ModuleExecutor mainExec = ModuleExecutor.getModuleExecutor(stack, this.runstate, main,
				main.getDefaultSubstitution(), RuleEvaluationOrder.LINEAR);
		mainExec.loadKR(); // required before receiving messages/percepts!
		this.runstate.startCycle(false);

		// If we have an event module, add it to the stack as
		// well, i.e. execute it before the main module.
		final Module event = this.runstate.getEventModule();
		if (event != null) {
			final ModuleExecutor eventExec = ModuleExecutor.getModuleExecutor(stack, this.runstate, event,
					event.getDefaultSubstitution(), RuleEvaluationOrder.LINEARALL);
			// eventExec.setSource(UseCase.EVENT);
			stack.push(eventExec);
		}
		// If we have an init module, add it to the stack as
		// well, i.e. execute it once before the other modules.
		final Module init = this.runstate.getInitModule();
		if (init != null) {
			final ModuleExecutor initExec = ModuleExecutor.getModuleExecutor(stack, this.runstate, init,
					init.getDefaultSubstitution(), RuleEvaluationOrder.LINEARALL);
			// initExec.setSource(UseCase.INIT);
			stack.push(initExec);
		}
		// Execute any possible init/event stuff.
		while (stack.canExecute()) {
			stack.pop();
			stack.getPopped().getResult();
		}

		// Execute all actions specified in the test.
		final ExecutionEventGeneratorInterface events = this.runstate.getEventGenerator();
		for (final TestAction action : this.test.getActions()) {
			TestResult exception = null;
			try {
				final TestActionExecutor execute = new TestActionExecutor(action);
				execute.run(this.runstate);
			} catch (final TestActionFailed e) {
				exception = e;
				events.event(DebugChannel.TESTFAILURE, null, null, exception.accept(new TestResultFormatter()));
				break;
			} catch (final ConditionFailed e) {
				exception = new EvaluationFailed("'" + action.getAction() + "' did not complete successfully.", e);
				events.event(DebugChannel.TESTFAILURE, null, null, exception.accept(new TestResultFormatter()));
				break;
			} catch (DebuggerKilledException | BoundaryException passthrough) {
			} finally {
				final ModuleTestExecutor current = this.testMap
						.get(agent.getController().getRunState().getActiveModule().getSignature());
				if (current != null) {
					this.tests.put(current, exception);
				}
			}
		}
		final AgentTestResult result = new AgentTestResult(this.test, getTestResults());
		agent.dispose(false);
		return result;
	}

	private List<ModuleTestResult> getTestResults() {
		final List<ModuleTestResult> results = new ArrayList<>(this.tests.size());
		for (final ModuleTestExecutor moduletest : this.tests.keySet()) {
			moduletest.destroy(this.runstate);

			final List<TestConditionResult> testResults = new ArrayList<>(moduletest.getExecutors().length);
			final List<ConditionFailed> failures = new ArrayList<>();
			for (final TestConditionExecutor test : moduletest.getExecutors()) {
				final TestConditionResult result = new TestConditionResult(test);
				final int existing = failures.indexOf(result.getFailure());
				if (existing != -1) {
					final ConditionFailed failure = failures.get(existing);
					failure.addEvaluation(result.getFailure().getFirstEvaluation());
				} else {
					testResults.add(result);
					if (result.getFailure() != null) {
						failures.add(result.getFailure());
					}
				}
			}

			results.add(new ModuleTestResult(moduletest, this.tests.get(moduletest), testResults));
		}
		return results;
	}

	@Override
	public void goalEvent(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource,
			final String message, final Object... args) {
		if (channel.equals(DebugChannel.MODULE_ENTRY)) {
			final Module module1 = (Module) associateObject;
			final ModuleTest test1 = this.parent.getModuleTest(module1.getSignature());
			if (test1 != null && !this.testMap.containsKey(module1.getSignature())) {
				final ModuleTestExecutor executor1 = new ModuleTestExecutor(test1);
				this.testMap.put(module1.getSignature(), executor1);
				executor1.install(this.runstate);
				this.tests.put(executor1, null);
			}
		}
	}

	@Override
	public int hashCode() {
		return (this.test == null) ? 0 : this.test.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof AgentTestExecutor)) {
			return false;
		}
		final AgentTestExecutor other = (AgentTestExecutor) obj;
		if (!Objects.equals(this.test, other.test)) {
			return false;
		}
		return true;
	}
}
