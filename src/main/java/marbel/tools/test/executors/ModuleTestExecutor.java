/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test.executors;

import java.util.Objects;

import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.KRInterface;
import marbel.languageTools.program.test.ModuleTest;
import marbel.languageTools.program.test.testcondition.TestCondition;
import marbel.tools.debugger.DebugEvent;
import marbel.tools.debugger.DebuggerKilledException;
import marbel.tools.errorhandling.Warning;
import marbel.tools.test.executors.testcondition.TestConditionExecutor;

public class ModuleTestExecutor extends TestObserver {
	private final ModuleTest test;

	public ModuleTestExecutor(final ModuleTest test) {
		this.test = test;
	}

	public ModuleTest getTest() {
		return this.test;
	}

	public void install(final RunState runstate) {
		initialize(runstate);

		// Install the in-conditions
		final KRInterface kr = runstate.getKRI();
		for (final TestCondition condition : this.test.getTestConditions()) {
			add(TestConditionExecutor.getTestConditionExecutor(condition, kr.getSubstitution(null), runstate, this));
		}
	}

	/**
	 * Will be called explicitly from the ModuleTestExecutor and AgentTestExecutor
	 * at takedown.
	 *
	 * @param runstate
	 */
	public void destroy(final RunState runstate) {
		// Allow in-conditions to clean-up after themselves (determine final
		// results, e.g. fail eventually or succeed always/never conditions)
		try {
			for (final TestConditionExecutor executor : getExecutors()) {
				executor.evaluate((DebugEvent) null);
			}
		} catch (final DebuggerKilledException e) {
			new Warning("Did not finish evaluating in-conditions of '" + getTest().getModuleName() + "' for agent '"
					+ runstate.getId() + "' because a timeout was reached.", e).emit();
		}
	}

	@Override
	public int hashCode() {
		return (this.test == null) ? 0 : this.test.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof ModuleTestExecutor)) {
			return false;
		}
		final ModuleTestExecutor other = (ModuleTestExecutor) obj;
		if (!Objects.equals(this.test, other.test)) {
			return false;
		}
		return true;
	}
}
