package marbel.tools.test.executors;

import java.util.ArrayList;
import java.util.List;

import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.parser.SourceInfo;
import marbel.tools.debugger.DebugEvent;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventListener;
import marbel.tools.test.executors.testcondition.TestConditionExecutor;

public abstract class TestObserver extends ExecutionEventListener { // DebugObserver
	private List<TestConditionExecutor> executors;

	protected void initialize(final RunState runstate) {
		this.executors = new ArrayList<>();
		runstate.getEventGenerator().addListener(this);
	}

	public void add(final TestConditionExecutor executor) {
		this.executors.add(executor);
	}

	public void remove(final TestConditionExecutor executor) {
		this.executors.remove(executor);
	}

	public TestConditionExecutor[] getExecutors() {
		return this.executors.toArray(new TestConditionExecutor[this.executors.size()]);
	}

	/**
	 * The channels that we listen to. Module entries might also add a set of
	 * beliefs/goals. Note: event module entry handles percept/mails as well Module
	 * exits might drop goals. Note: main module exit means agent exit
	 */
	private final static List<DebugChannel> channels = List.of(DebugChannel.ACTION_EXECUTED_BUILTIN,
			DebugChannel.ACTION_EXECUTED_MESSAGING, DebugChannel.MODULE_ENTRY, DebugChannel.MODULE_EXIT);

	@Override
	public void goalEvent(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource,
			final String message, final Object... args) {
		if (!channels.contains(channel)) {
			return;
		}
		for (final TestConditionExecutor executor : getExecutors()) {
			// FIXME Bit of a hack to reconnect the evaluate function
			// maybe we can do without the DebugEvent here?
			executor.evaluate(new DebugEvent(null, null, channel, associateObject, associateSource, message, args));
		}
	}
}
