/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test.executors.testcondition;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import marbel.core.runtime.mentalstate.MentalStateConditionExecutor;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.test.testcondition.Always;
import marbel.languageTools.program.test.testcondition.Eventually;
import marbel.languageTools.program.test.testcondition.Never;
import marbel.languageTools.program.test.testcondition.TestCondition;
import marbel.languageTools.program.test.testcondition.Until;
import marbel.preferences.CorePreferences;
import marbel.tools.debugger.DebugEvent;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.debugger.events.NoEventGenerator;
import marbel.tools.errorhandling.Warning;
import marbel.tools.test.exceptions.ConditionFailed;
import marbel.tools.test.exceptions.EvaluationFailed;
import marbel.tools.test.executors.ModuleTestExecutor;
import marbel.tools.test.executors.TestActionExecutor;
import marbel.tools.test.executors.TestObserver;
import marbel.tools.test.result.TestResultFormatter;

/**
 * Abstract base for any test condition. Test conditions are evaluated in the
 * context of a running agent and need to provide an evaluator that can do so.
 */
public abstract class TestConditionExecutor {
	/**
	 * A test condition can have one out of three evaluations:
	 * {@link TestConditionEvaluation#PASSED},
	 * {@link TestConditionEvaluation#FAILED}, or
	 * {@link TestConditionEvaluation#UNKNOWN}.
	 */
	public enum TestConditionEvaluation {
		/**
		 * PASSED means that the test condition has been passed (holds).
		 */
		PASSED,
		/**
		 * FAILED means that the test condition failed during the test (did not hold).
		 */
		FAILED,
		/**
		 * UNKNOWN means that the test has not yet been completed and the test condition
		 * has not yet been completely evaluated.
		 */
		UNKNOWN;

		@Override
		public String toString() {
			switch (this) {
			case FAILED:
				return "failed";
			case PASSED:
				return "passed";
			case UNKNOWN:
				// if we don't know whether test condition was passed or not,
				// this means the test must have been interrupted.
				return "interrupted";
			default:
				// should never happen...
				return "unknown";
			}
		}
	}

	public enum TestEvaluationChannel {
		MODULE_ENTRY, MODULE_EXIT, ACTION_EXECUTED, STOPTEST;

		public static TestEvaluationChannel fromDebugChannel(final DebugChannel debug) {
			switch (debug) {
			case ACTION_EXECUTED_BUILTIN:
			case ACTION_EXECUTED_MESSAGING:
				return ACTION_EXECUTED;
			case MODULE_ENTRY:
				return MODULE_ENTRY;
			case MODULE_EXIT:
				return MODULE_EXIT;
			default:
				return STOPTEST;
			}
		}
	}

	private TestConditionEvaluation passed = TestConditionEvaluation.UNKNOWN;
	private ConditionFailed failure = null;
	protected final Substitution substitution;
	protected final RunState runstate;
	protected final TestObserver parent;
	protected final ExecutionEventGeneratorInterface noevents = new NoEventGenerator();
	protected DebugEvent current;

	public TestConditionExecutor(final Substitution substitution, final RunState runstate, final TestObserver parent) {
		this.substitution = substitution;
		this.runstate = runstate;
		this.parent = parent;
	}

	public String getContext() {
		if (this.parent instanceof ModuleTestExecutor) {
			final ModuleTestExecutor executor = (ModuleTestExecutor) this.parent;
			return (executor.getTest() == null) ? "unknown module"
					: ("module '" + executor.getTest().getModuleSignature() + "'");
		} else if (this.parent instanceof TestActionExecutor) {
			final TestActionExecutor executor = (TestActionExecutor) this.parent;
			return (executor.getAction() == null) ? "unknown action" : ("action '" + executor.getAction() + "'");
		} else {
			return "";
		}
	}

	public Substitution getSubstitution() {
		return this.substitution;
	}

	/**
	 * Get the parsed {@link TestCondition}.
	 *
	 * @return {@link TestCondition}
	 */
	abstract public TestCondition getCondition();

	/**
	 * Evaluates a mental state query on the agent's {@link RunState}.
	 *
	 * @param runstate     of the agent.
	 * @param substitution the current substitution.
	 * @param query        the mental state query.
	 * @return result of evaluating the mental state query.
	 */
	protected List<Substitution> evaluate() {
		final Substitution temp = this.substitution.clone();
		final MentalStateWithEvents mentalState = this.runstate.getMentalState();
		final MentalLiteral msc = getCondition().getQuery();

		List<Substitution> result = new ArrayList<>(0);
		try {
			result = new MentalStateConditionExecutor(msc, temp).evaluate(mentalState, this.noevents).getAnswers();
		} catch (final KRQueryFailedException e) {
			new Warning("testcondition evaluation of '" + msc + "' failed.", e).emit();
		}

		return result;
	}

	public void evaluate(final DebugEvent event) {
		if (this.passed == TestConditionEvaluation.UNKNOWN) {
			this.current = event;
			final TestEvaluationChannel channel = (event == null || event.getChannel() == null)
					? TestEvaluationChannel.STOPTEST
					: TestEvaluationChannel.fromDebugChannel(event.getChannel());
			try {
				// calls setPassed when applicable
				evaluate(channel, (event == null) ? null : event.getAssociatedSource());
			} catch (final ConditionFailed failure) {
				this.failure = failure;
				if (CorePreferences.getAbortOnTestFailure()) {
					throw failure;
				} else {
					final EvaluationFailed exception = new EvaluationFailed(
							"the test condition '" + getCondition() + "' failed.", failure);
					this.runstate.getEventGenerator().event(DebugChannel.TESTFAILURE, null, null,
							exception.accept(new TestResultFormatter()));
				}
			}
		}
	}

	abstract void evaluate(TestEvaluationChannel channel, SourceInfo info);

	/**
	 * Use this method for setting evaluation of test condition to either
	 * {@link TestConditionEvaluation#PASSED} or
	 * {@link TestConditionEvaluation#FAILED}.
	 *
	 * @param passed {@code true} to set evaluation to
	 *               {@link TestConditionEvaluation#PASSED}; {@code false} to set
	 *               evaluation to {@link TestConditionEvaluation#FAILED}.
	 */
	protected void setPassed(final boolean passed) {
		if (passed) {
			this.passed = TestConditionEvaluation.PASSED;
			this.parent.remove(this);
		} else {
			this.passed = TestConditionEvaluation.FAILED;
		}
	}

	/**
	 * @return true iff the test condition is passed
	 */
	public boolean isPassed() {
		return this.passed == TestConditionEvaluation.PASSED;
	}

	public TestConditionEvaluation getState() {
		return this.passed;
	}

	public ConditionFailed getFailure() {
		return this.failure;
	}

	public static TestConditionExecutor getTestConditionExecutor(final TestCondition condition,
			final Substitution substitution, final RunState runstate, final TestObserver parent) {
		if (condition instanceof Always) {
			return new AlwaysExecutor((Always) condition, substitution, runstate, (ModuleTestExecutor) parent);
		} else if (condition instanceof Eventually) {
			return new EventuallyExecutor((Eventually) condition, substitution, runstate, (ModuleTestExecutor) parent);
		} else if (condition instanceof Never) {
			return new NeverExecutor((Never) condition, substitution, runstate, (ModuleTestExecutor) parent);
		} else if (condition instanceof Until) {
			return new UntilExecutor((Until) condition, substitution, runstate, (TestActionExecutor) parent);
		} else {
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = getClass().hashCode();
		result = prime * result + ((getContext() == null) ? 0 : getContext().hashCode());
		result = prime * result + ((getCondition() == null) ? 0 : getCondition().hashCode());
		return prime * result + ((this.substitution == null) ? 0 : this.substitution.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final TestConditionExecutor other = (TestConditionExecutor) obj;
		if (getContext() == null) {
			if (other.getContext() != null) {
				return false;
			}
		} else if (!getContext().equals(other.getContext())) {
			return false;
		}
		if (getCondition() == null) {
			if (other.getCondition() != null) {
				return false;
			}
		} else if (!getCondition().equals(other.getCondition())) {
			return false;
		}
		if (!Objects.equals(this.substitution, other.getSubstitution())) {
			return false;
		}
		return true;
	}
}
