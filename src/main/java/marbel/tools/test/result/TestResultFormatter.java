/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test.result;

import java.util.Map.Entry;

import marbel.languageTools.program.agent.AgentId;
import marbel.tools.test.exceptions.ConditionFailed;
import marbel.tools.test.exceptions.EvaluationFailed;
import marbel.tools.test.exceptions.TestActionFailed;

public class TestResultFormatter implements ResultFormatter<String> {
	private String indent(final int level) {
		final StringBuilder ret = new StringBuilder();
		for (int i = 0; i < level; i++) {
			ret.append("  ");
		}
		return ret.toString();
	}

	private String indent(final int level, final String text) {
		final String ident = indent(level);
		return ident + text.replaceAll("\n", "\n" + ident).trim();
	}

	private String indent(final String text) {
		return indent(1, text);
	}

	/**
	 * Formats a {@link TestProgramResult} by listing all agents that their test
	 * status. If an agent failed its tests, more details are printed.
	 *
	 * @param testRunResult
	 * @return
	 */
	@Override
	public String visit(final TestProgramResult testProgramResult) {
		String ret;
		if (testProgramResult.isPassed()) {
			ret = "\ntest passed:\n";
		} else {
			ret = "\ntest failed:\n";
		}
		ret += indent("test: " + testProgramResult.getUnitTestFile()) + "\n";
		for (final Entry<AgentId, TestInterpreterResult> tr : testProgramResult.getResults().entrySet()) {
			ret += indent(formatGroup(tr.getKey().toString(), tr.getValue())) + "\n";
		}
		return ret;
	}

	/**
	 * Formats a list of {@link AgentTestResult}s. If the list is empty the test did
	 * not run and a message if printed. If the test contains one result the list is
	 * printed concisely. Otherwise the full list is printed.
	 *
	 * @param groupName
	 * @param results
	 * @param indent
	 * @return
	 */
	private String formatGroup(final String groupName, final TestInterpreterResult results) {
		final StringBuilder ret = new StringBuilder(groupName).append(":\n");
		ret.append(indent(results.accept(this))).append("\n");
		return ret.toString();
	}

	/**
	 * Formats a {@link AgentTestResult}. If passed the result is formatted as a
	 * single line. If the test failed all {@link AssertTestResult}s are printed.
	 *
	 * @param result
	 * @return
	 */
	@Override
	public String visit(final TestInterpreterResult result) {
		final StringBuilder ret = new StringBuilder();
		if (result.getUncaughtThrowable() != null) {
			ret.append(getCause(result.getUncaughtThrowable())).append("\n");
		}
		if (result.hasTimedOut()) {
			ret.append("the test timeout was reached!\n");
		}
		if (result.getResult() != null) {
			ret.append(result.getResult().accept(this));
		}
		return ret.toString();
	}

	@Override
	public String visit(final AgentTestResult result) {
		String ret = "";
		for (final ModuleTestResult test : result.getTestResults()) {
			ret += test.accept(this);
		}
		if (ret.trim().isEmpty()) {
			return "all conditions were evaluated succesfully.";
		} else {
			return ret + "\n";
		}
	}

	@Override
	public String visit(final ModuleTestResult result) {
		final StringBuilder ret = new StringBuilder();
		for (final TestConditionResult sub : result.getResults()) {
			ret.append(sub.accept(this));
		}
		if (result.getException() != null) {
			ret.append("\n\texception: ").append(result.getException().accept(this));
		}
		return ret.toString();
	}

	@Override
	public String visit(final TestConditionResult result) {
		if (result.isPassed()) {
			return "";
		} else {
			final StringBuilder ret = new StringBuilder();
			if (result.getFailure() == null) {
				ret.append("unknown failure");
			} else {
				ret.append(result.getFailure().getMessage());
			}
			return ret.append("\n").toString();
		}
	}

	@Override
	public String visit(final TestActionFailed taf) {
		String ret = taf.getMessage();
		ret += getCause(taf.getCause());
		return ret;
	}

	@Override
	public String visit(final EvaluationFailed ef) {
		String ret = ef.getMessage();
		ret += getCause(ef.getCause());
		return ret;
	}

	@Override
	public String visit(final ConditionFailed cf) {
		String ret = cf.getMessage();
		ret += getCause(cf.getCause());
		return ret;
	}

	private String getCause(Throwable cause) {
		final StringBuilder ret = new StringBuilder();
		while (cause != null) {
			if (cause.getMessage() == null) {
				cause.printStackTrace();
			} else {
				ret.append("\n\tbecause: ").append(cause.getMessage());
			}
			cause = cause.getCause();
		}
		return ret.toString();
	}
}
