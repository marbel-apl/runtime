/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test.exceptions;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.test.testcondition.TestCondition;
import marbel.tools.errorhandling.exceptions.GOALRuntimeErrorException;
import marbel.tools.test.result.ResultFormatter;
import marbel.tools.test.result.TestResult;

public class ConditionFailed extends GOALRuntimeErrorException implements TestResult {
	private static final long serialVersionUID = -1899022697249102491L;
	private final TestCondition condition;
	private final String context;
	private final Map<Substitution, List<Substitution>> evaluations;
	private final SourceInfo info;
	private final boolean timedOut;

	public ConditionFailed(final TestCondition condition, final String context, final Substitution subst,
			final List<Substitution> evaluation, final SourceInfo info, final boolean timedOut) {
		super("");
		this.condition = condition;
		this.context = context;
		this.evaluations = new LinkedHashMap<>();
		addEvaluation(new AbstractMap.SimpleEntry<>(subst, evaluation));
		this.info = info;
		this.timedOut = timedOut;
	}

	public Map.Entry<Substitution, List<Substitution>> getFirstEvaluation() {
		return this.evaluations.entrySet().iterator().next();
	}

	public void addEvaluation(final Map.Entry<Substitution, List<Substitution>> evaluation) {
		if (this.evaluations.containsKey(evaluation.getKey())) {
			this.evaluations.get(evaluation.getKey()).addAll(evaluation.getValue());
		} else {
			this.evaluations.put(evaluation.getKey(), evaluation.getValue());
		}
	}

	private String getCondition() {
		return this.condition.toString().replace("\n", " ");
	}

	@Override
	public String getMessage() {
		final StringBuilder msg = new StringBuilder();
		msg.append("The condition '").append(getCondition()).append("'");
		if (this.context != null) {
			msg.append(" in ").append(this.context);
		}
		msg.append(" was violated");
		if (this.timedOut) {
			msg.append(" after the timeout occurred");
		}
		if (this.info != null) {
			msg.append(" at ").append(this.info);
		}
		msg.append(":");
		final Iterator<Substitution> substs = this.evaluations.keySet().iterator();
		while (substs.hasNext()) {
			final Substitution subst = substs.next();
			final List<Substitution> evaluation = this.evaluations.get(subst);
			if (evaluation.isEmpty()) {
				msg.append(" no evaluation");
			} else {
				msg.append(" the evaluation ").append(evaluation);
			}
			msg.append(" applied");
			if (!subst.getVariables().isEmpty()) {
				msg.append(" for ").append(subst);
			}
			if (substs.hasNext()) {
				msg.append("; ");
			}
		}
		msg.append(".");
		return msg.toString();
	}

	@Override
	public <T> T accept(final ResultFormatter<T> formatter) {
		return formatter.visit(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = this.timedOut ? 1 : 0;
		result = prime * result + ((this.condition == null) ? 0 : this.condition.hashCode());
		result = prime * result + ((this.context == null) ? 0 : this.context.hashCode());
		return prime * result + ((this.info == null) ? 0 : this.info.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof ConditionFailed)) {
			return false;
		}
		final ConditionFailed other = (ConditionFailed) obj;
		if (!Objects.equals(this.condition, other.condition) || !Objects.equals(this.context, other.context)
				|| !Objects.equals(this.info, other.info)) {
			return false;
		}
		return (this.timedOut == other.timedOut);
	}
}
