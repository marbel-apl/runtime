package marbel.tools.test.exceptions;

import marbel.tools.test.result.ResultFormatter;
import marbel.tools.test.result.TestResult;

public class EvaluationFailed extends Exception implements TestResult {
	/** Generated serialVersionUID */
	private static final long serialVersionUID = -5176959367012313106L;

	public EvaluationFailed(final String message, final Exception cause) {
		super(message, cause);
	}

	@Override
	public <T> T accept(final ResultFormatter<T> formatter) {
		return formatter.visit(this);
	}
}
