package marbel.tools.eclipse;

import java.util.Objects;

import marbel.krInterface.parser.SourceInfo;

class StackInfo {
	private final String name;
	private final SourceInfo source;

	public StackInfo(final String name, final SourceInfo source) {
		this.name = name;
		this.source = source;
	}

	public String getName() {
		return this.name;
	}

	public SourceInfo getSource() {
		return this.source;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.name, this.source);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof StackInfo)) {
			return false;
		}
		final StackInfo other = (StackInfo) obj;
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name) || !Objects.equals(this.source, other.source)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		if (this.name != null) {
			builder.append(this.name).append(" ");
		}
		if (this.source != null) {
			builder.append("at ").append(this.source);
		}
		return builder.toString();
	}
}
