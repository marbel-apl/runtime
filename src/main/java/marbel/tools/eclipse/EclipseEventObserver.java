/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.eclipse;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import eis.iilang.EnvironmentState;
import marbel.core.agent.Agent;
import marbel.core.runtime.RuntimeEvent;
import marbel.core.runtime.RuntimeEventObserver;
import marbel.core.runtime.RuntimeManager;
import marbel.core.runtime.service.environment.EnvironmentPort;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.AgentId;
import marbel.tools.BreakpointManager;
import marbel.tools.IDEGOALInterpreter;
import marbel.tools.debugger.DebugEvent;
import marbel.tools.debugger.IDEDebugger;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.ExecutionEventListener;
import marbel.tools.eclipse.DebugCommand.Command;

public class EclipseEventObserver implements RuntimeEventObserver {
	private final Map<AgentId, EclipseDebugObserver> observers;
	private final BreakpointManager mngr;
	private InputReaderWriter writer;
	private EnvironmentPort environment = null;

	public EclipseEventObserver(final BreakpointManager mngr) {
		this.observers = new ConcurrentHashMap<>();
		this.mngr = mngr;
	}

	public void setWriter(final InputReaderWriter writer) {
		this.writer = writer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void eventOccured(final RuntimeManager<?, ?> observable, final RuntimeEvent event) {
		switch (event.getType()) {
		case ENVIRONMENT_LAUNCHED:
			this.environment = (EnvironmentPort) event.getSource();
			this.writer.write(new DebugCommand(Command.ENV_CREATED, this.environment,
					this.environment.getEnvironmentState().name()));
			break;
		case ENVIRONMENT_RUNMODE_CHANGED:
			final EnvironmentState state = this.environment.getEnvironmentState();
			if (state != EnvironmentState.INITIALIZING) {
				this.writer.write(new DebugCommand(Command.ENV_STATE, this.environment, state.name()));
			}
			break;
		case AGENT_IS_LOCAL_AND_READY:
			final Agent<IDEGOALInterpreter> agent = (Agent<IDEGOALInterpreter>) event.getSource();
			final EclipseDebugObserver debugobserver = new EclipseDebugObserver(agent, this.writer);
			debugobserver.subscribe();
			this.observers.put(agent.getId(), debugobserver);
			final IDEDebugger debugger = agent.getController().getDebugger();
			debugger.setBreakpoints(this.mngr.getBreakpoints());
			debugobserver.notifyBreakpointHit(new DebugEvent(debugger.getRunMode(), getClass().getSimpleName(),
					DebugChannel.RUNMODE, null, null));
			break;
		default:
			break;
		}
	}

	public EclipseDebugObserver getObserver(final Agent<IDEGOALInterpreter> agent) {
		return this.observers.get(agent.getId());
	}

	public static class EclipseEventListener extends ExecutionEventListener {
		private final EclipseDebugObserver observer;

		public EclipseEventListener(final EclipseDebugObserver observer) {
			this.observer = observer;
		}

		@Override
		public void goalEvent(final DebugChannel channel, final Object associateObject,
				final SourceInfo associateSource, final String message, final Object... args) {
			this.observer.notifyBreakpointHit(new DebugEvent(null, getClass().getSimpleName(), channel, associateObject,
					associateSource, message, args));
		}
	}
}
