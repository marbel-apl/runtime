/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.eclipse;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import marbel.core.agent.Agent;
import marbel.core.runtime.mentalstate.MentalStateResult;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.AgentId;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.preferences.CorePreferences;
import marbel.preferences.DebugPreferences;
import marbel.preferences.LoggingPreferences;
import marbel.tools.IDEGOALInterpreter;
import marbel.tools.debugger.DebugEvent;
import marbel.tools.debugger.DebugObserver;
import marbel.tools.debugger.IDEDebugger;
import marbel.tools.debugger.SteppingDebugger.RunMode;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.eclipse.DebugCommand.Command;

public class EclipseDebugObserver implements DebugObserver {
	private final Agent<IDEGOALInterpreter> agent;
	private final InputReaderWriter writer;
	private final EclipseStepper stepper;
	private final Deque<StackInfo> stack;
	private SourceInfo source;

	/**
	 * Handles events from an {@link Agent} to put pre-defined output on a
	 * {@link InputReaderWriter}
	 *
	 * @param agent  The {@link Agent}.
	 * @param writer The {@link InputReaderWriter}.
	 */
	public EclipseDebugObserver(final Agent<IDEGOALInterpreter> agent, final InputReaderWriter writer) {
		this.agent = agent;
		this.writer = writer;
		this.stepper = new EclipseStepper(agent);
		this.stack = new ArrayDeque<>();
		this.source = agent.getController().getProgram().getSourceInfo();
	}

	public void processCommand(final DebugCommand c) {
		this.stepper.processCommand(c);
	}

	/**
	 * Subscribe to everything we want to listen to
	 */
	public void subscribe() {
		final IDEDebugger debugger = this.agent.getController().getDebugger();
		for (final DebugChannel channel : DebugChannel.values()) {
			// Listen to all channels to update the agent's source position
			debugger.subscribe(this, channel);
		}
		this.writer.write(new DebugCommand(Command.LAUNCHED, this.agent.getId(), debugger.getRunMode().name()));
	}

	@Override
	public String getObserverName() {
		return getClass().getSimpleName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean notifyBreakpointHit(final DebugEvent event) {
		if (event.getChannel().getLevel() > 0 && event.getChannel().getLevel() < Integer.MAX_VALUE
				&& event.getAssociatedSource() != null) {
			this.source = event.getAssociatedSource();
			updateStack(event);
		} else if (event.getChannel() == DebugChannel.MODULE_EXIT) {
			updateStack(event);
		}
		final AgentId agentId = this.agent.getId();
		if (DebugPreferences.getChannelState(event.getChannel()).canView()
				&& LoggingPreferences.getEclipseAgentConsoles()) {
			this.writer.write(new DebugCommand(Command.LOG, agentId, event.toString()));
		}
		switch (event.getChannel()) {
		case RUNMODE:
			final RunMode runmode = event.getRunMode();
			switch (runmode) {
			case RUNNING:
				this.writer.write(new DebugCommand(Command.RUNMODE, agentId, runmode.name()));
				break;
			case PAUSED:
				this.writer.write(new DebugCommand(Command.RUNMODE, agentId, runmode.name()));
				suspendAtSource();
				break;
			case KILLED:
				this.writer.write(new DebugCommand(Command.RUNMODE, agentId, runmode.name()));
				if (!CorePreferences.getRemoveKilledAgent()) {
					suspendAtSource();
				}
				break;
			default:
				break;
			}
			break;
		case CLEARSTATE:
			this.writer.write(new DebugCommand(Command.CLEAR, agentId));
			break;
		case BREAKPOINTS:
			this.source = event.getAssociatedSource();
			break;
		case MODULE_ENTRY:
			final List<String> mAsList1 = new ArrayList<>(2);
			final Module module1 = (Module) event.getRawArguments()[0];
			mAsList1.add(module1.toString());
			final Substitution subst1 = (Substitution) event.getRawArguments()[1];
			mAsList1.add(subst1.toString());
			this.writer.write(new DebugCommand(Command.MODULE_ENTRY, agentId, mAsList1));
			break;
		case MODULE_EXIT:
			final List<String> mAsList2 = new ArrayList<>(1);
			final Module module2 = (Module) event.getRawArguments()[0];
			mAsList2.add(module2.toString());
			this.writer.write(new DebugCommand(Command.MODULE_EXIT, agentId, mAsList2));
			break;
		case KR_UPDATES:
			final MentalStateResult formulas = (MentalStateResult) event.getAssociatedObject();
			for (final DatabaseFormula removed : formulas.getRemoved()) {
				this.writer.write(new DebugCommand(Command.DELETED_BEL, agentId, removed.toString()));
			}
			for (final DatabaseFormula added : formulas.getAdded()) {
				this.writer.write(new DebugCommand(Command.INSERTED_BEL, agentId, added.toString()));
			}
			break;
		case RULE_CONDITION_EVALUATION:
			final List<String> rAsList = new ArrayList<>();
			if (event.getRawArguments().length > 1) {
				final List<Substitution> substset = (List<Substitution>) event.getRawArguments()[1];
				switch (substset.size()) {
				case 0:
					rAsList.add("[]");
					break;
				case 1:
					final Substitution rSub = substset.iterator().next();
					for (final Var var : rSub.getVariables()) {
						rAsList.add(var + "/" + rSub.get(var));
					}
					if (rAsList.isEmpty()) {
						rAsList.add("[]");
					}
					break;
				default:
					for (final Substitution sub : substset) {
						rAsList.add(sub.toString());
					}
					break;

				}
			} else {
				rAsList.add("no solutions");
			}
			this.writer.write(new DebugCommand(Command.RULE_EVALUATION, agentId, rAsList));
			break;
		case CALL_ACTION_OR_MODULE:
			final List<String> cAsList = new ArrayList<>();
			final Substitution cSub = (Substitution) event.getRawArguments()[event.getRawArguments().length - 1];
			for (final Var var : cSub.getVariables()) {
				cAsList.add(var + "/" + cSub.get(var));
			}
			if (cAsList.isEmpty()) {
				cAsList.add("[]");
			}
			this.writer.write(new DebugCommand(Command.RULE_EVALUATION, agentId, cAsList));
			break;
		case ACTION_EXECUTED_USERSPEC:
			if (LoggingPreferences.getEclipseActionHistory()) {
				final Action<?> executed = (Action<?>) event.getRawArguments()[0];
				if (!(executed instanceof ModuleCallAction)) {
					final StringBuilder actionlog = new StringBuilder(executed.toString());
					if (LoggingPreferences.getIncludeStackInLogs()) {
						final List<String> actionstack = new ArrayList<>();
						for (final StackInfo call : this.stack.toArray(new StackInfo[this.stack.size()])) {
							final File source = new File(call.getSource().getSource());
							actionstack.add(source.getName() + ":" + call.getSource().getLineNumber());
						}
						actionlog.append(" ").append(actionstack);
					}
					this.writer.write(new DebugCommand(Command.EXECUTED, agentId, actionlog.toString()));
				}
			}
			break;
		default:
			break;
		}
		// Let the EclipseStepper process the event
		return this.stepper.processEvent(event);
	}

	private void updateStack(final DebugEvent event) {
		switch (event.getChannel()) {
		case MODULE_ENTRY:
			final String module = "Module " + event.getAssociatedObject();
			this.stack.push(new StackInfo(module, event.getAssociatedSource()));
			break;
		case MODULE_EXIT:
			final Module main = this.agent.getController().getProgram().getMainModule();
			if (main == null || !event.getAssociatedObject().toString().equals(main.toString())) {
				this.stack.pop();
			}
			break;
		default:
			if (!this.stack.isEmpty()) {
				final StackInfo toUpdate = this.stack.pop();
				this.stack.push(new StackInfo(toUpdate.getName(), this.source));
			}
			break;
		}
	}

	/**
	 * Send a message to the stream to suspend the agent at its last known source
	 * position (code that has been run)
	 */
	public void suspendAtSource() {
		final List<String> params = new ArrayList<>();
		for (final StackInfo call : this.stack.toArray(new StackInfo[this.stack.size()])) {
			final SourceInfo source = call.getSource();
			final int end = source.getCharacterPosition() + source.getStopIndex() - source.getStartIndex();
			final String param = call.getName() + "#" + source.getSource() + "#" + source.getLineNumber() + "#"
					+ source.getCharacterPosition() + "#" + end;
			params.add(param);
		}
		this.writer.write(new DebugCommand(Command.SUSPEND, this.agent.getId(), params));
	}

	/**
	 * Send a message to the stream to suspend the agent at the given source
	 * position
	 */
	public void suspendAtSource(final String name, final SourceInfo source) {
		final List<String> params = new ArrayList<>(1);
		final int end = source.getCharacterPosition() + source.getStopIndex() - source.getStartIndex();
		final String param = name + "#" + source.getSource() + "#" + source.getLineNumber() + "#"
				+ source.getCharacterPosition() + "#" + end;
		params.add(param);
		this.writer.write(new DebugCommand(Command.SUSPEND, this.agent.getId(), params));
	}
}