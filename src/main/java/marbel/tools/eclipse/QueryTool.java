/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.eclipse;

import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import marbel.cognitiveKr.CognitiveKR;
import marbel.core.agent.Agent;
import marbel.core.agent.GOALInterpreter;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.errors.Message;
import marbel.languageTools.errors.module.ModuleErrorStrategy;
import marbel.languageTools.parser.MOD2GLexer;
import marbel.languageTools.parser.MOD2GParser;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.actions.ExitModuleAction;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.languageTools.program.agent.actions.UserSpecAction;
import marbel.languageTools.program.agent.actions.UserSpecOrModuleCall;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.tools.debugger.SteppingDebugger;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;
import marbel.tools.errorhandling.exceptions.GOALException;
import marbel.tools.errorhandling.exceptions.GOALUserError;

public class QueryTool {
	private final Agent<? extends GOALInterpreter<?>> agent;

	public QueryTool(final Agent<? extends GOALInterpreter<?>> agent) {
		this.agent = agent;
	}

	public String doquery(final String userEnteredQuery) throws GOALUserError {
		final MentalLiteral msc = parseMSC(userEnteredQuery);
		final Object debugger = this.agent.getController().getDebugger();
		if (debugger instanceof SteppingDebugger) {
			final Substitution[] substitutions = ((SteppingDebugger) debugger).query(msc);
			if (substitutions == null) {
				return "Query could not be executed";
			} else if (substitutions.length == 0) {
				return "No solutions";
			} else {
				final StringBuilder resulttext = new StringBuilder();
				for (final Substitution s : substitutions) {
					resulttext.append(s).append("\n");
				}
				return resulttext.toString();
			}
		} else {
			return "Unexpected debugger: " + debugger;
		}
	}

	public String doaction(final String userEnteredAction) throws GOALUserError, GOALActionFailedException {
		final ActionCombo action = parseAction(userEnteredAction);
		final MentalStateWithEvents mentalState = this.agent.getController().getRunState().getMentalState();
		if (mentalState == null) {
			throw new GOALUserError("agent '" + this.agent.getId() + "' has not yet initialized its databases");
		} else {
			this.agent.getController().doPerformAction(action);
			return "Executed " + action;
		}
	}

	/**
	 * Creates an embedded module parser that can parse the given string.
	 *
	 * @param pString is the string to be parsed.
	 * @return a MOD2GParser.
	 */
	private MOD2GParser prepareModuleParser(final ModuleValidator validator, final String pString) {
		final CharStream charstream = CharStreams.fromString(pString, validator.getFilename());
		final MOD2GLexer lexer = new MOD2GLexer(charstream);
		lexer.removeErrorListeners();
		lexer.addErrorListener(validator);
		final MOD2GParser parser = new MOD2GParser(new CommonTokenStream(lexer));
		parser.setErrorHandler(new ModuleErrorStrategy());
		parser.removeErrorListeners();
		parser.addErrorListener(validator);
		return parser;
	}

	/**
	 * Parse a string to a {@link MentalStateCondition}.
	 *
	 * @param mentalStateCondition Input string that should represent a mental state
	 *                             condition.
	 * @return The mental state condition that resulted from parsing the input
	 *         string.
	 * @throws GOALException   When the parser throws a RecognitionException, which
	 *                         should have been buffered and ignored.
	 * @throws ParserException
	 */
	public MentalLiteral parseMSC(final String mentalStateCondition) throws GOALUserError {
		// Try to parse the MSC.
		final AgentDefinition agent = this.agent.getController().getProgram();
		final FileRegistry tempRegistry = new FileRegistry(agent.getRegistry());
		final ModuleValidator sub = new ModuleValidator("query-condition", tempRegistry);
		final Module temp = new Module(tempRegistry, null);
		temp.setKRInterface(agent.getKRInterface());
		sub.overrideProgram(temp);

		try {
			final CognitiveKR ckr = sub.getCognitiveKR();
			final Query bel = ckr.visitQuery(mentalStateCondition, null);
			final MentalLiteral msc = new MentalLiteral(bel, ckr.getUsedSignatures(bel), null);

			// check for errors in the parser
			if (!tempRegistry.getAllErrors().isEmpty()) {
				final StringBuilder msg = new StringBuilder("'").append(mentalStateCondition)
						.append("' cannot be parsed: ");
				for (final Message err : tempRegistry.getAllErrors()) {
					msg.append(err.toString()).append(". ");
				}
				throw new GOALUserError(msg.toString());
			}

			return msc;
		} catch (final ParserException e) {
			throw new GOALUserError("failed to parse as query", (Exception) e);
		}
	}

	/**
	 * Parse string as a mental action.
	 */
	@SuppressWarnings("unchecked")
	public ActionCombo parseAction(final String action) throws GOALUserError {
		// Try to parse the action
		final AgentDefinition agent = this.agent.getController().getProgram();
		final FileRegistry tempRegistry = new FileRegistry(agent.getRegistry());
		final ModuleValidator sub = new ModuleValidator("query-action", tempRegistry);
		final Module temp = new Module(agent.getRegistry(), null);
		temp.setKRInterface(agent.getKRInterface());
		sub.overrideProgram(temp);

		final MOD2GParser parser = prepareModuleParser(sub, action);
		final ActionCombo combo = sub.visitActioncombo(parser.actioncombo());

		// Module calls are not allowed in the query tool, so in case the action
		// is not a mental action we will assume it is a user-specified action
		// that can be send to the environment.
		if (combo == null || combo.size() == 0) {
			throw new GOALUserError("not a valid action");
		} else {
			final ActionCombo returned = new ActionCombo(null);
			for (final Action<?> act : combo.getActions()) {
				if (act instanceof UserSpecOrModuleCall) {
					returned.addAction(new UserSpecAction(act.getName(), (List<Term>) act.getParameters(), null));
				} else if ((!(act instanceof ExitModuleAction) && !(act instanceof ModuleCallAction))) {
					returned.addAction(act);
				} else {
					throw new GOALUserError("cannot call module actions here.");
				}
			}
			return returned;
		}
	}
}
