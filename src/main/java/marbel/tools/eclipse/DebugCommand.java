/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.eclipse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import eis.iilang.Parameter;
import marbel.core.runtime.service.environment.EnvironmentPort;
import marbel.languageTools.program.agent.AgentId;

public class DebugCommand {
	private final static String PREFIX = "DC";
	private final static String DELIMITER = "|";

	public enum Command {
		// received
		PAUSE, RUN, STEP, BREAKS, STOP,
		// received and/or sent
		EVAL,
		// sent (1/2)
		RUNMODE, LOG, LAUNCHED, SUSPEND, INSERTED_BEL, DELETED_BEL,
		// sent (2/2)
		RULE_EVALUATION, MODULE_ENTRY, MODULE_EXIT, EXECUTED, CLEAR,
		// environment
		ENV_CREATED, ENV_STATE, ENV_PAUSE, ENV_RUN,
		// history
		HISTORY_STATE, HISTORY_STEP, HISTORY_FORWARD, HISTORY_BACK,
		// explanation
		WHY_ACTION, WHY_NOT_ACTION;
	}

	private final Command command;
	private final AgentId agent;
	private final EnvironmentPort environment;
	private final List<String> data;

	public DebugCommand(final Command command, final List<String> data) {
		this.command = command;
		this.agent = null;
		this.environment = null;
		this.data = data;
	}

	public DebugCommand(final Command command, final String data) {
		this(command, new ArrayList<String>(1));
		if (!data.isEmpty()) {
			this.data.add(data);
		}
	}

	public DebugCommand(final Command command, final AgentId agent, final List<String> data) {
		this.command = command;
		this.agent = agent;
		this.environment = null;
		this.data = data;
	}

	public DebugCommand(final Command command, final AgentId agent, final String data) {
		this(command, agent, new ArrayList<String>(1));
		if (!data.isEmpty()) {
			this.data.add(data);
		}
	}

	public DebugCommand(final Command command, final AgentId agent) {
		this(command, agent, new ArrayList<String>(0));
	}

	public DebugCommand(final Command command, final EnvironmentPort environment, final List<String> data) {
		this.command = command;
		this.agent = null;
		this.environment = environment;
		this.data = data;
	}

	public DebugCommand(final Command command, final EnvironmentPort environment, final String data) {
		this(command, environment, new ArrayList<String>(1));
		if (!data.isEmpty()) {
			this.data.add(data);
		}
	}

	public DebugCommand(final Command command, final EnvironmentPort environment) {
		this(command, environment, new ArrayList<String>(0));
	}

	public Command getCommand() {
		return this.command;
	}

	public AgentId getAgent() {
		return this.agent;
	}

	public EnvironmentPort getEnvironment() {
		return this.environment;
	}

	public List<String> getAllData() {
		return Collections.unmodifiableList(this.data);
	}

	public String getData(final int index) {
		return this.data.get(index);
	}

	public String getData() {
		if (this.data.isEmpty()) {
			return "";
		} else {
			return getData(0);
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof DebugCommand)) {
			return false;
		} else {
			final DebugCommand other = (DebugCommand) obj;
			if (!Objects.equals(this.agent, other.agent)) {
				return false;
			}
			if (this.environment == null) {
				if (other.environment != null) {
					return false;
				}
			} else if (!this.environment.equals(this.environment) || (this.command != other.command)
					|| !Objects.equals(this.data, other.data)) {
				return false;
			}
			return true;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.agent, this.environment, this.command, this.data);
	}

	@Override
	public String toString() {
		final StringBuilder buffer = new StringBuilder(PREFIX);
		final String agentName = (this.agent == null) ? "" : this.agent.toString();
		final String agent = agentName.replace('\n', ' ').replace(DELIMITER, "\\" + DELIMITER);
		final String envName = (this.environment == null) ? "" : this.environment.toString();
		final String environment = envName.replace('\n', ' ').replace(DELIMITER, "\\" + DELIMITER);
		buffer.append(DELIMITER).append(this.command.name()).append(DELIMITER).append(agent).append(DELIMITER)
				.append(environment).append(DELIMITER).append(this.data.size());
		for (final String data : this.data) {
			final String d = data.replace("\n", " --lb-- ").replace(DELIMITER, "\\" + DELIMITER);
			buffer.append(DELIMITER).append(d);
		}
		return buffer.toString();
	}

	public static DebugCommand fromString(final String string) {
		if (string.startsWith(PREFIX)) {
			final String[] s = string.split("(?<!\\\\)\\" + DELIMITER);
			if (s.length >= 5) {
				final Command command = Command.valueOf(s[1]);
				final AgentId agent = s[2].isEmpty() ? null : new AgentId(s[2].replace("\\" + DELIMITER, DELIMITER));
				final EnvironmentPort environment = s[3].isEmpty() ? null
						: new StubEnvironment(s[3].replace("\\" + DELIMITER, DELIMITER));
				final int size = Integer.parseInt(s[4]);
				final List<String> data = new ArrayList<>(size);
				for (int i = 5; i < (size + 5); i++) {
					data.add(s[i].replace("\\" + DELIMITER, DELIMITER).replace("--lb--", "\n"));
				}
				if (agent != null) {
					return new DebugCommand(command, agent, data);
				} else if (environment != null) {
					return new DebugCommand(command, environment, data);
				} else {
					return new DebugCommand(command, data);
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static class StubEnvironment extends EnvironmentPort {
		public StubEnvironment(final String name) {
			super(null, name, new HashMap<String, Parameter>(0));
		}
	}
}
