package marbel.tools.debugger.events;

import java.util.ArrayList;
import java.util.List;

import marbel.krInterface.parser.SourceInfo;

/**
 * A source of agent execution ("breakpoint") events.
 */
public class ExecutionEventGenerator implements ExecutionEventGeneratorInterface {
	/**
	 * Ensures the order is fixed.
	 */
	private final List<ExecutionEventListener> listeners = new ArrayList<>();

	@Override
	public void event(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource,
			final String message, final Object... args) {
		for (final ExecutionEventListener l : this.listeners
				.toArray(new ExecutionEventListener[this.listeners.size()])) {
			l.goalEvent(channel, associateObject, associateSource, message, args);
		}
	}

	@Override
	public void event(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource) {
		for (final ExecutionEventListener l : this.listeners
				.toArray(new ExecutionEventListener[this.listeners.size()])) {
			l.goalEvent(channel, associateObject, associateSource);
		}
	}

	@Override
	public void addListener(final ExecutionEventListener l) {
		this.listeners.add(l);
	}

	@Override
	public void removeListener(final ExecutionEventListener l) {
		this.listeners.remove(l);
	}

	@Override
	public void clearListeners() {
		this.listeners.clear();
	}
}
