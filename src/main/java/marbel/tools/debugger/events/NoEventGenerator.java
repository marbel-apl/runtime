package marbel.tools.debugger.events;

import marbel.krInterface.parser.SourceInfo;

/**
 * Stub for ExecutionEventGeneratorInterface that completely ignores the calls.
 * Used when queries are done to the agent's mental state for introspection,
 * which should not be charged on the agent.
 */
public class NoEventGenerator implements ExecutionEventGeneratorInterface {
	@Override
	public void event(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource,
			final String message, final Object... args) {
	}

	@Override
	public void event(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource) {
	}

	@Override
	public void addListener(final ExecutionEventListener l) {
	}

	@Override
	public void removeListener(final ExecutionEventListener l) {
	}

	@Override
	public void clearListeners() {
	}
}
