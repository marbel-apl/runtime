package marbel.tools.debugger;

import marbel.tools.debugger.events.DebugChannel;

public class NOPObserver implements DebugObserver {
	private final ObservableDebugger debugger;

	public NOPObserver(final ObservableDebugger debugger) {
		this.debugger = debugger;
	}

	public void subscribe() {
		this.debugger.subscribe(this, DebugChannel.NONE);
	}

	@Override
	public String getObserverName() {
		return getClass().getSimpleName();
	}

	@Override
	public boolean notifyBreakpointHit(final DebugEvent event) {
		return true;
	}
}
