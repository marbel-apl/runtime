/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.debugger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import marbel.core.agent.GOALInterpreter;
import marbel.core.runtime.RuntimeManager;
import marbel.core.runtime.service.environment.EnvironmentPort;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.AgentId;
import marbel.tools.debugger.events.DebugChannel;

public class ObservableDebugger extends SteppingDebugger {
	/**
	 * Maintains a map of which observers have subscribed to which channels.
	 */
	protected final Map<DebugChannel, Set<DebugObserver>> channelObservers;

	public ObservableDebugger(final AgentId id,
			final RuntimeManager<?, ? extends GOALInterpreter<? extends SteppingDebugger>> manager,
			final EnvironmentPort env) {
		this(id.toString(), manager, env);
	}

	public ObservableDebugger(final String id,
			final RuntimeManager<?, ? extends GOALInterpreter<? extends SteppingDebugger>> manager,
			final EnvironmentPort env) {
		super(id, manager, env);
		// Initialize channel to observer mapping.
		final DebugChannel[] channels = DebugChannel.values();
		this.channelObservers = new HashMap<>(channels.length);
		for (final DebugChannel channel : channels) {
			this.channelObservers.put(channel, new HashSet<DebugObserver>());
		}
	}

	@Override
	public void breakpoint(final DebugChannel channel, final Object associateObject, final SourceInfo associateSource,
			final String message, final Object... args) {
		// Only if there are observers for the channel, events need to be send.
		if (!this.channelObservers.get(channel).isEmpty()) {
			final DebugEvent event = new DebugEvent(getRunMode(), getName(), channel, associateObject, associateSource,
					message, args);
			if (!notifyObservers(channel, event)) {
				return;
			}
		}

		super.breakpoint(channel, associateObject, associateSource, message, args);
	}

	/**
	 * Updates observers that subscribed to a channel with the debug information
	 * related to that channel.
	 *
	 * @param channel Channel to send information on.
	 * @param event   Debug event provided to observers subscribed to the channel.
	 */
	protected boolean notifyObservers(final DebugChannel channel, final DebugEvent event) {
		boolean result = true;
		for (final DebugObserver observer : this.channelObservers.get(channel)) {
			result &= observer.notifyBreakpointHit(event);
		}
		return result;
	}

	@Override
	protected void setRunMode(final RunMode mode) {
		// notify observers of run mode change.
		if (mode != getRunMode()) {
			notifyObservers(DebugChannel.RUNMODE,
					new DebugEvent(mode, getName(), DebugChannel.RUNMODE, mode, null, "run mode is now '%s'.", mode));
		}
		super.setRunMode(mode);
	}

	/**
	 * add channel to viewed channels. Your observer will be notified when debug
	 * event happens on that channel.
	 *
	 * @param observer
	 * @param channel
	 *
	 * @throws NullPointerException If the given observer is not subscribed to this
	 *                              {@link SteppingDebugger} (or the given observer
	 *                              is a {@link BreakpointObserver}).
	 */
	public void subscribe(final DebugObserver observer, final DebugChannel channel) {
		this.channelObservers.get(channel).add(observer);
	}

	/**
	 * <p>
	 * Removes the observer from the list of registered observers.
	 * </p>
	 *
	 * @param observer observer to be removed from registered observer list.
	 */
	public void unsubscribe(final DebugObserver observer) {
		for (final DebugChannel channel : this.channelObservers.keySet()) {
			this.channelObservers.get(channel).remove(observer);
		}
	}

	/**
	 * remove channel from viewed channels. Your observer not will be notified
	 * anymore when debug event happens on that channel.
	 *
	 * @param observer is observer that wants to stop viewing the channel
	 * @param channel  is channel to be removed from view.
	 *
	 * @throws NullPointerException If the given observer is not subscribed to this
	 *                              {@link SteppingDebugger} (or the given observer
	 *                              is a {@link BreakpointObserver}).
	 */
	public void unsubscribe(final DebugObserver observer, final DebugChannel channel) {
		this.channelObservers.get(channel).remove(observer);
	}

	/**
	 * Check if observer is viewing given channel.
	 *
	 * @param observer is observer that might be viewing
	 * @param channel  is channel that might be under observation.
	 * @return true if under observation, false if not.
	 *
	 * @throws NullPointerException If the given observer is not subscribed to this
	 *                              {@link SteppingDebugger} (or the given observer
	 *                              is a {@link BreakpointObserver}).
	 */
	public boolean isViewing(final DebugObserver observer, final DebugChannel channel) {
		return this.channelObservers.get(channel).contains(observer);
	}

	@Override
	protected boolean checkUserBreakpointHit(final SourceInfo source, final DebugChannel channel) {
		final boolean hit = super.checkUserBreakpointHit(source, channel);
		if (hit) {
			final DebugEvent event = new DebugEvent(getRunMode(), getName(), DebugChannel.BREAKPOINTS, null, source,
					"hit a user-defined breakpoint.");
			notifyObservers(DebugChannel.BREAKPOINTS, event);
		}
		return hit;
	}

	@Override
	public String toString() {
		return super.toString() + "\nObservers per channel:\n" + this.channelObservers.toString();
	}
}
