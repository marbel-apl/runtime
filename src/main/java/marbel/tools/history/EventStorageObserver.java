package marbel.tools.history;

import marbel.core.agent.Agent;
import marbel.core.agent.GOALInterpreter;
import marbel.core.runtime.mentalstate.MentalStateResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.NonMentalAction;
import marbel.tools.debugger.DebugEvent;
import marbel.tools.debugger.DebugObserver;
import marbel.tools.debugger.ObservableDebugger;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.history.events.ActionEvent;
import marbel.tools.history.events.CallEvent;
import marbel.tools.history.events.EnterEvent;
import marbel.tools.history.events.InspectionEvent;
import marbel.tools.history.events.LeaveEvent;
import marbel.tools.history.events.ModificationAction;
import marbel.tools.history.events.ModificationEvent;

/**
 * Observer to fill the {@link EventStorage}.
 */
public class EventStorageObserver implements DebugObserver {
	private final EventStorage storage;
	private final GOALInterpreter<?> controller;
	private SourceInfo source;

	public EventStorageObserver(final EventStorage storage, final Agent<GOALInterpreter<?>> agent) {
		this.storage = storage;
		this.controller = agent.getController();
		this.source = this.controller.getProgram().getSourceInfo();
	}

	@Override
	public String getObserverName() {
		return getClass().getSimpleName();
	}

	public void subscribe() {
		if (this.controller.getDebugger() instanceof ObservableDebugger) {
			final ObservableDebugger debugger = (ObservableDebugger) this.controller.getDebugger();
			for (final DebugChannel channel : DebugChannel.values()) {
				// Listen to all channels to update the agent's source position
				debugger.subscribe(this, channel);
			}
		}
	}

	@Override
	public boolean notifyBreakpointHit(final DebugEvent event) {
		final RunState runState = this.controller.getRunState();
		if (event.getChannel().getLevel() > 0 && event.getChannel().getLevel() < Integer.MAX_VALUE
				&& event.getAssociatedSource() != null) {
			this.source = event.getAssociatedSource();
		}
		switch (event.getChannel()) {
		case KR_UPDATES:
			final MentalStateResult result = (MentalStateResult) event.getAssociatedObject();
			final ModificationAction modification = new ModificationAction(runState, result.getAdded(),
					result.getRemoved(), this.source);
			this.storage.write(new ModificationEvent(modification));
			break;
		case MODULE_ENTRY:
			final Module module1 = (Module) event.getAssociatedObject();
			this.storage.write(new EnterEvent(runState, module1));
			break;
		case MODULE_EXIT:
			final Module module2 = (Module) event.getAssociatedObject();
			this.storage.write(new LeaveEvent(runState, module2, null));
			break;
		case RULE_CONDITION_EVALUATION:
			final MentalLiteral condition = (MentalLiteral) event.getAssociatedObject();
			this.storage.write(new InspectionEvent(runState, condition));
			break;
		case CALL_ACTION_OR_MODULE:
			final Action<?> called = (Action<?>) event.getRawArguments()[0];
			final Substitution subst = (Substitution) event.getRawArguments()[1];
			if (called instanceof NonMentalAction) {
				this.storage.write(new CallEvent(runState, called, subst));
			}
			break;
		case ACTION_EXECUTED_USERSPEC:
		case ACTION_EXECUTED_BUILTIN:
			final Action<?> executed = (Action<?>) event.getAssociatedObject();
			if (executed instanceof NonMentalAction) {
				this.storage.write(new ActionEvent(runState, executed));
			}
			break;
		default:
			break;
		}
		return true;
	}
}
