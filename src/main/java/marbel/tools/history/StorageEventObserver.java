package marbel.tools.history;

import java.util.HashMap;
import java.util.Map;

import marbel.core.agent.Agent;
import marbel.core.agent.GOALInterpreter;
import marbel.core.runtime.RuntimeEvent;
import marbel.core.runtime.RuntimeEventObserver;
import marbel.core.runtime.RuntimeManager;
import marbel.languageTools.program.agent.AgentId;
import marbel.tools.errorhandling.Warning;

/**
 * Observer that creates a {@link EventStorage} and associated
 * {@link EventStorageObserver} when neeeded.
 */
public class StorageEventObserver implements RuntimeEventObserver {
	private static final Map<AgentId, EventStorage> storages = new HashMap<>();

	public StorageEventObserver() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void eventOccured(final RuntimeManager<?, ?> observable, final RuntimeEvent event) {
		switch (event.getType()) {
		case MAS_BORN:
			storages.clear();
			break;
		case AGENT_IS_LOCAL_AND_READY:
			final Agent<GOALInterpreter<?>> agent = (Agent<GOALInterpreter<?>>) event.getSource();
			final EventStorage storage = new EventStorage(agent.getId());
			storages.put(agent.getId(), storage);
			final EventStorageObserver observer = new EventStorageObserver(storage, agent);
			observer.subscribe();
			break;
		case MAS_DIED:
			for (final EventStorage history : storages.values()) {
				try {
					history.finish(true);
				} catch (final InterruptedException e) {
					new Warning("unclean shutdown of agent history", e).emit();
				}
			}
		default:
			break;
		}
	}

	public static EventStorage getHistory(final AgentId agent) {
		return storages.get(agent);
	}
}
