package marbel.tools.history.events;

import java.util.ArrayList;
import java.util.List;

import marbel.core.runtime.mentalstate.MentalStateConditionResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.parser.ParsedObject;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.ProgramMap;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.tools.debugger.events.NoEventGenerator;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

public class InspectionEvent extends AbstractEvent {
	private final int msc;

	public InspectionEvent(final RunState runState, final MentalLiteral msc) {
		this.msc = runState.getMap().getIndex(msc.getSourceInfo());
	}

	@Override
	public SourceInfo getSource(final ProgramMap map) {
		return map.getObject(this.msc).getSourceInfo();
	}

	@Override
	public List<String> getLookupData(final ProgramMap map) {
		final List<String> result = new ArrayList<>();
		final MentalLiteral msc = getMentalStateCondition(map);
		if (msc != null) {
			result.addAll(msc.getUsedSignatures());
		}
		return result;
	}

	public MentalLiteral getMentalStateCondition(final ProgramMap map) {
		final ParsedObject get = map.getObject(this.msc);
		if (get instanceof MentalLiteral) {
			return (MentalLiteral) get;
		} else {
			return null;
		}
	}

	@Override
	public void execute(final RunState runState, final boolean reverse) throws GOALActionFailedException {
		// nothing to do here...
	}

	@Override
	public String getDescription(final RunState runState) {
		final MentalLiteral msc = (MentalLiteral) runState.getMap().getObject(this.msc);
		try {
			final MentalStateConditionResult result = runState.getMentalState().evaluate(msc,
					runState.getKRI().getSubstitution(null), new NoEventGenerator());
			return result.holds() ? ("Query held with " + result.getAnswers()) : "Query did not hold";
		} catch (final KRQueryFailedException e) {
			return e.getMessage();
		}
	}

	@Override
	public int hashCode() {
		return this.msc;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof InspectionEvent)) {
			return false;
		}
		final InspectionEvent other = (InspectionEvent) obj;
		return (this.msc == other.msc);
	}
}
