package marbel.tools.history.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.ParsedObject;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.ProgramMap;
import marbel.languageTools.program.agent.actions.Action;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

public class CallEvent extends AbstractEvent {
	private final int call;
	private final Substitution subst;

	public CallEvent(final RunState runState, final Action<?> call, final Substitution subst) {
		this.call = runState.getMap().getIndex(call.getSourceInfo());
		this.subst = subst;
	}

	@Override
	public SourceInfo getSource(final ProgramMap map) {
		return map.getObject(this.call).getSourceInfo();
	}

	@Override
	public List<String> getLookupData(final ProgramMap map) {
		final Action<?> action = getAction(map);
		if (action == null) {
			return new ArrayList<>(0);
		} else {
			final List<String> result = new ArrayList<>(1);
			result.add(action.getSignature());
			return result;
		}
	}

	public Action<?> getAction(final ProgramMap map) {
		final ParsedObject get = map.getObject(this.call);
		return (get instanceof Action<?>) ? (Action<?>) get : null;
	}

	public Substitution getSubstitution() {
		return this.subst;
	}

	@Override
	public void execute(final RunState runState, final boolean reverse) throws GOALActionFailedException {
		// nothing to do here...
	}

	@Override
	public String getDescription(final RunState runState) {
		return "Called action with " + this.subst;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		final int result = this.call;
		return prime * result + ((this.subst == null) ? 0 : this.subst.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || !(obj instanceof CallEvent)) {
			return false;
		}
		final CallEvent other = (CallEvent) obj;
		if ((this.call != other.call) || !Objects.equals(this.subst, other.subst)) {
			return false;
		}
		return true;
	}
}
