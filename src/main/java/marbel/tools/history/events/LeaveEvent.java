
package marbel.tools.history.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.ParsedObject;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.ProgramMap;
import marbel.languageTools.program.agent.Module;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

public class LeaveEvent extends AbstractEvent {
	private final int source;
	private final Substitution parameters;

	public LeaveEvent(final RunState runState, final Module module, final Substitution subst) {
		this.source = runState.getMap().getIndex(module.getDefinition());
		this.parameters = subst;
	}

	@Override
	public SourceInfo getSource(final ProgramMap map) {
		return ((Module) map.getObject(this.source)).getDefinition();
	}

	@Override
	public List<String> getLookupData(final ProgramMap map) {
		final List<String> result = new ArrayList<>(1);
		final ParsedObject get = map.getObject(this.source);
		if (get instanceof Module) {
			result.add(((Module) get).getSignature());
		}
		return result;
	}

	@Override
	public void execute(final RunState runState, final boolean reverse) throws GOALActionFailedException {
		final Module enter = (Module) runState.getMap().getObject(this.source);
		if (reverse) {
			runState.enterModule(enter);
		} else {
			runState.exitModule(enter);
		}
		// TODO: use parameter context
	}

	@Override
	public String getDescription(final RunState runState) {
		return "Left module";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		final int result = this.source;
		return prime * result + ((this.parameters == null) ? 0 : this.parameters.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof LeaveEvent)) {
			return false;
		}
		final LeaveEvent other = (LeaveEvent) obj;
		if (!Objects.equals(this.parameters, other.parameters)) {
			return false;
		}
		return (this.source == other.source);
	}
}
