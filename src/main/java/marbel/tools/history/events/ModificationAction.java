package marbel.tools.history.events;

import java.util.List;
import java.util.Objects;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKr.TranslationException;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.core.executors.actions.ActionExecutor;
import marbel.core.runtime.mentalstate.MentalStateResult;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.KRInterface;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Update;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.ProgramMap;
import marbel.languageTools.program.agent.actions.DeleteAction;
import marbel.languageTools.program.agent.actions.InsertAction;
import marbel.languageTools.program.agent.actions.MentalAction;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

// Note that this class is quite similar to mentalState.Result
public class ModificationAction {
	private final List<DatabaseFormula> added;
	private final List<DatabaseFormula> removed;
	private final int info;

	public ModificationAction(final RunState runState, final List<DatabaseFormula> added,
			final List<DatabaseFormula> removed, final SourceInfo info) {
		this.added = added;
		this.removed = removed;
		this.info = runState.getMap().getIndex(info);
	}

	public List<DatabaseFormula> getAdded() {
		return this.added;
	}

	public List<DatabaseFormula> getRemoved() {
		return this.removed;
	}

	public SourceInfo getSourceInfo(final ProgramMap map) {
		return map.getObject(this.info).getSourceInfo();
	}

	public void execute(final RunState runState, final boolean reverse) throws GOALActionFailedException {
		final KRInterface kri = runState.getKRI();
		// TODO: use module/action context instead of empty subst
		final Substitution empty = kri.getSubstitution(null);
		final SourceInfo info = getSourceInfo(runState.getMap());
		try {
			final CognitiveKR translator = CognitiveKRFactory.getCognitiveKR(kri);
			if (!this.added.isEmpty()) {
				final Update added = translator.makeUpdate(this.added);
				final MentalAction action1 = reverse ? new DeleteAction(added, info) : new InsertAction(added, info);
				final ActionExecutor exec1 = ActionExecutor.getActionExecutor(action1, empty);
				exec1.execute(runState);
			}
			if (!this.removed.isEmpty()) {
				final Update removed = translator.makeUpdate(this.removed);
				final MentalAction action2 = reverse ? new InsertAction(removed, info)
						: new DeleteAction(removed, info);
				final ActionExecutor exec2 = ActionExecutor.getActionExecutor(action2, empty);
				exec2.execute(runState);
			}
		} catch (final InstantiationFailedException | TranslationException e1) {
			throw new GOALActionFailedException("failed to execute modification.", e1);
		}
	}

	@Override
	public String toString() {
		final MentalStateResult result = new MentalStateResult();
		result.added(this.added);
		result.removed(this.removed);
		final String returned = result.toString();
		return returned.substring(0, returned.length() - 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = this.info;
		result = prime * result + ((this.added == null) ? 0 : this.added.hashCode());
		return prime * result + ((this.removed == null) ? 0 : this.removed.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof ModificationAction)) {
			return false;
		}
		final ModificationAction other = (ModificationAction) obj;
		if (!Objects.equals(this.added, other.added) || !Objects.equals(this.removed, other.removed)) {
			return false;
		}
		return (this.info == other.info);
	}
}
