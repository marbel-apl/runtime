
package marbel.tools.history.explanation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import marbel.krInterface.KRInterface;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Update;
import marbel.krInterface.parser.ParsedObject;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.ProgramMap;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.rules.Rule;
import marbel.tools.history.EventStorage;
import marbel.tools.history.events.AbstractEvent;
import marbel.tools.history.events.ActionEvent;
import marbel.tools.history.events.CallEvent;
import marbel.tools.history.events.InspectionEvent;
import marbel.tools.history.events.ModificationAction;
import marbel.tools.history.events.ModificationEvent;
import marbel.tools.history.explanation.reasons.ActionReason;
import marbel.tools.history.explanation.reasons.ActionRuleCondition;
import marbel.tools.history.explanation.reasons.NoActionNeverApplied;
import marbel.tools.history.explanation.reasons.NoActionNeverEvaluated;
import marbel.tools.history.explanation.reasons.NoActionNeverSatisfied;
import marbel.tools.history.explanation.reasons.NoActionReason;
import marbel.tools.history.explanation.reasons.Reason;

/**
 * Supports generating explanations ({@link Reason}s) from an agent trace based
 * on the 'Debugging is Explaining' paper (Hindriks 2012).
 */
public class DebuggingIsExplaining {
	private final EventStorage trace;
	private final ProgramMap map;
	private Set<DatabaseFormula> formulas;
	private Set<Action<?>> actions;

	public DebuggingIsExplaining(final EventStorage trace, final ProgramMap map) {
		this.trace = trace;
		this.map = map;
	}

	/**
	 * Call this to update the info returned by {@link #getAllActions()},
	 * {@link #getAllFormulas()}, and {@link #getAllGoals()}.
	 */
	public void process() {
		this.formulas = new LinkedHashSet<>();
		this.actions = new LinkedHashSet<>();
		if (this.trace == null) {
			return;
		}
		for (final AbstractEvent event : this.trace.getAll()) {
			if (event instanceof InspectionEvent) {
				final InspectionEvent inspection = (InspectionEvent) event;
				final Update query = inspection.getMentalStateCondition(this.map).getFormula().toUpdate();
				this.formulas.addAll(query.getAddList());
				this.formulas.addAll(query.getDeleteList());
			} else if (event instanceof ModificationEvent) {
				final ModificationAction modification = ((ModificationEvent) event).getUpdate();
				this.formulas.addAll(modification.getAdded());
				this.formulas.addAll(modification.getRemoved());
			} else if (event instanceof CallEvent) {
				final Action<?> called = ((CallEvent) event).getAction(this.map);
				if (called != null) {
					this.actions.add(called);
				}
			} else if (event instanceof ActionEvent) {
				final Action<?> executed = ((ActionEvent) event).getAction(this.map);
				if (executed != null) {
					this.actions.add(executed);
				}
			}
		}
	}

	/**
	 * @return All beliefs (instantiated or not) that can be found in the trace,
	 *         either queried or actually believed at some point. Call
	 *         {@link #process()} first to update the returned beliefs.
	 */
	public Set<DatabaseFormula> getAllFormulas() {
		return Collections.unmodifiableSet(this.formulas);
	}

	/**
	 * @return All actions (instantiated or not) that can be found in the trace,
	 *         either tried or actually executed at some point. Call
	 *         {@link #process()} first to update the returned actions.
	 */
	public Set<Action<?>> getAllActions() {
		return Collections.unmodifiableSet(this.actions);
	}

	/**
	 * @param action An action from which we want to know why it was excuted.
	 * @return One or more {@link ActionReason}s (if the action was actually
	 *         executed) explaining why this action was executed (each entry
	 *         corresponds to one successful execution).
	 */
	public List<Reason> whyAction(final Action<?> action, final KRInterface kri) {
		final List<Reason> reasons = new ArrayList<>();
		if (this.trace == null) {
			return reasons;
		}
		Action<?> executed = null;
		ActionReason current = null;
		Substitution subst = null;
		for (int i = (this.trace.getMax() - 1); i >= 0; --i) {
			final AbstractEvent event = this.trace.getAll().get(i);
			if (event instanceof ActionEvent) {
				executed = ((ActionEvent) event).getAction(this.map);
			} else if (executed != null && event instanceof CallEvent) {
				final CallEvent call = (CallEvent) event;
				subst = call.getSubstitution();
				executed = executed.applySubst(subst);
				if (action.mgu(executed, kri) != null) {
					current = new ActionReason(executed, i);
				}
				executed = null;
			} else if (current != null && event instanceof InspectionEvent) {
				final InspectionEvent inspection = (InspectionEvent) event;
				final MentalLiteral msc = inspection.getMentalStateCondition(this.map);
				final ActionRuleCondition pre = new ActionRuleCondition(msc, subst, i);
				current.setRuleCondition(pre);
				reasons.add(current);
				current = null;
			}
		}
		return reasons;
	}

	/**
	 * @param action An action from which we want to know why it was excuted.
	 * @return A list containing a single {@link NoActionReason} (if the action was
	 *         never actually executed; one or more {@link ActionReason}s otherwise,
	 *         see {@link DebuggingIsExplaining#whyAction(Action)}) explaining why
	 *         this action was not executed.
	 */
	public List<Reason> whyNotAction(final Action<?> action, final KRInterface kri) {
		final List<Reason> reasons = whyAction(action, kri);
		if (reasons.isEmpty() && this.trace != null) {
			final Set<SourceInfo> related = new LinkedHashSet<>();
			for (final ParsedObject object : this.map.getAll()) {
				if (object instanceof Module) {
					final Module module = (Module) object;
					for (final Rule rule : module.getRules()) {
						for (final Action<?> call : rule.getAction()) {
							if (call.getSignature().equals(action.getSignature())) {
								related.add(rule.getCondition().getSourceInfo());
								break;
							}
						}
					}
				}
			}
			final Set<Action<?>> otherInstances = new LinkedHashSet<>();
			boolean preEvaluated = false, ruleEvaluated = false;
			for (final AbstractEvent event : this.trace.getAll()) {
				if (event instanceof CallEvent) {
					final CallEvent call = (CallEvent) event;
					final Action<?> called = (call.getAction(this.map) == null) ? null
							: call.getAction(this.map).applySubst(call.getSubstitution());
					if (action.mgu(called, kri) != null) {
						preEvaluated = true;
						break;
					} else if (called != null && action.getSignature().equals(called.getSignature())) {
						otherInstances.add(called);
					}
				} else if (!ruleEvaluated && event instanceof InspectionEvent) {
					final MentalLiteral condition = ((InspectionEvent) event).getMentalStateCondition(this.map);
					if (related.contains(condition.getSourceInfo())) {
						ruleEvaluated = true;
					}
				}
			}
			NoActionReason noAction = null;
			if (preEvaluated) {
				final NoActionNeverSatisfied neverSatisfied = new NoActionNeverSatisfied(action);
				neverSatisfied.setRuleSatisfied();
				noAction = neverSatisfied;
			} else if (otherInstances.size() > 0) {
				final NoActionNeverApplied neverApplied = new NoActionNeverApplied(action);
				neverApplied.setOtherInstances(otherInstances);
				noAction = neverApplied;
			} else if (ruleEvaluated) {
				final NoActionNeverSatisfied neverSatisfied = new NoActionNeverSatisfied(action);
				noAction = neverSatisfied;
			} else {
				final NoActionNeverEvaluated neverEvaluated = new NoActionNeverEvaluated(action);
				neverEvaluated.setRelatedRules(related);
				noAction = neverEvaluated;
			}
			reasons.add(noAction);
		}
		return reasons;
	}
}
