package marbel.tools.history.explanation.reasons;

import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.MentalLiteral;

/**
 * Part of an {@link ActionReason}: a rule(condition) selecting that action held
 * with a certain substitution.
 */
public class ActionRuleCondition extends Reason {
	private final MentalLiteral msc;
	private final Substitution subst;

	public ActionRuleCondition(final MentalLiteral msc, final Substitution subst, final int state) {
		super(msc.getSourceInfo(), state);
		this.msc = msc;
		this.subst = subst;
	}

	public Substitution getSubstitution() {
		return this.subst;
	}

	@Override
	public String toString() {
		final StringBuilder string = new StringBuilder();
		string.append("the rule condition '").append(this.msc).append("' held with ").append(this.subst).append(" at <")
				.append(this.location).append("> in state ").append(this.state);
		return string.toString();
	}
}
