package marbel.tools.history.explanation.reasons;

import marbel.languageTools.program.agent.actions.Action;

/**
 * Explains why an action was performed. Exists of both an
 * {@link ActionPreCondition} and an {@link ActionRuleCondition}.
 */
public class ActionReason extends Reason {
	private final Action<?> action;
	private ActionRuleCondition sub;

	public ActionReason(final Action<?> action, final int state) {
		super(action.getSourceInfo(), state);
		this.action = action;
	}

	public void setRuleCondition(final ActionRuleCondition sub) {
		this.sub = sub;
	}

	public boolean hasRuleConditionReason() {
		return (this.sub != null);
	}

	@Override
	public String toString() {
		final StringBuilder string = new StringBuilder();
		string.append(this.action).append(" was executed in state ").append(this.state).append(" because ")
				.append(this.sub).append(".");
		return string.toString();
	}
}
