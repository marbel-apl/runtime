/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools;

import marbel.core.agent.AgentRegistry;
import marbel.core.agent.GOALInterpreter;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.tools.debugger.IDEDebugger;
import marbel.tools.profiler.Profiles;

public class IDEGOALInterpreter extends GOALInterpreter<IDEDebugger> {
	public IDEGOALInterpreter(final AgentDefinition program, final AgentRegistry<?> registry,
			final IDEDebugger debugger, final Profiles profiles) {
		super(program, registry, debugger, profiles);
	}
}