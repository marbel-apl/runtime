/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.preferences;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.DebugChannel.ChannelState;

/**
 * Stores the users/application's debug preferences. Some apps have very strict
 * settings (GOAL-Eclipse), others (SimpleIDE) allow modifying most.
 */
public class DebugPreferences {
	private static List<PropertyChangeListener> listeners = new ArrayList<>();
	private static Map<String, Object> preferences;
	private static Map<String, Object> defaultSettings = null;

	/**
	 * This function must be called to initialize GOAL, before running.
	 *
	 * @param defaultSet the default debug settings for GOAL. Used when there is no
	 *                   available preferences file
	 */

	public static void setDefault(final Map<String, Object> defaultSet) {
		for (final DebugChannel channel : DebugChannel.values()) {
			final String valStr = (String) defaultSet.get(channel.name());
			if (valStr == null) {
				throw new IllegalArgumentException("defaultSet is missing value for key=" + channel.name());
			}
			try {
				ChannelState.valueOf(valStr);
			} catch (final IllegalArgumentException e) {
				throw new IllegalArgumentException("defaultSet value " + valStr + "is not a proper value ");
			}
		}
		defaultSettings = defaultSet;
	}

	/**
	 * Force given settings to use in the system. Will be called by Preferences
	 *
	 * @param init the actual settings, actually a Map<String,String>. If null, we
	 *             load the default settings.
	 */
	public static void initPrefs(final Map<String, Object> init) {
		if (defaultSettings == null) {
			throw new IllegalStateException("DebugPreferences.setDefault has not been called");
		}
		if (init == null) {
			preferences = new TreeMap<>(defaultSettings);
		} else {
			preferences = init;
		}
	}

	/**
	 * Reset all channels to default state.
	 */
	public static void reset() {
		for (final DebugChannel c : DebugChannel.values()) {
			setChannelState(c, ChannelState.valueOf((String) defaultSettings.get(c.name())));
		}
	}

	public static Map<String, Object> getPrefs() {
		return Collections.unmodifiableMap(preferences);
	}

	/**
	 * Gets the preferred {@link ChannelState} for the given {@link DebugChannel}.
	 *
	 * @param channel The {@link DebugChannel} to get the state for.
	 * @return The preferred {@link ChannelState} of the given {@link DebugChannel},
	 *         or the default state for the given channel if there is no preference.
	 */
	public static ChannelState getChannelState(final DebugChannel channel) {
		return get(channel);
	}

	/**
	 * Sets the {@link ChannelState} for the given {@link DebugChannel}.
	 *
	 * @param channel The channel to set the state for.
	 * @param state   The desired state of the channel.
	 */
	public static void setChannelState(final DebugChannel channel, final ChannelState state) {
		final String oldValue = getChannelState(channel).name();
		put(channel, state);

		// Notify listeners of changes.
		for (final PropertyChangeListener listener : listeners) {
			listener.propertyChange(new PropertyChangeEvent(channel, channel.toString(), oldValue, state.toString()));
		}
	}

	/**
	 * Subscribe a listener to the Debug preferences, so that it will get a
	 * notification via a call to when the preferences are changed.
	 *
	 * @param listener
	 */
	public static void addChangeListener(final PropertyChangeListener listener) {
		listeners.add(listener);
	}

	/**
	 * unsubscribe as listener
	 *
	 * @param listener
	 */
	public static void removeChangeListener(final PropertyChangeListener listener) {
		listeners.remove(listener);
	}

	// 3 helper functions...
	private static ChannelState get(final DebugChannel pref) {
		if (preferences == null) {
			Preferences.initializeAllPrefs();
		}
		final String get = (String) preferences.get(pref.name());
		if (get == null || get.isEmpty()) {
			return ChannelState.NONE;
		} else {
			return ChannelState.valueOf(get);
		}
	}

	private static void put(final DebugChannel pref, final ChannelState value) {
		if (preferences == null) {
			Preferences.initializeAllPrefs();
		}
		preferences.put(pref.name(), value.name());
	}

	/**
	 * Hide constructor.
	 */
	private DebugPreferences() {
	}

}