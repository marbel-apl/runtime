/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.preferences;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import marbel.tools.profiler.InfoType;

public class ProfilerPreferences {
	public enum Pref {
		profiling, profilingToFile, logNodeID
	}

	private static Map<String, Object> preferences;

	/**
	 * Initializes the preference settings. If no initial preference settings are
	 * provided, the default preference settings are used (as if user did not change
	 * any settings).
	 *
	 * @param init The settings for initializing the preferences.
	 */
	public static void initPrefs(final Map<String, Object> init) {
		if (init == null) {
			preferences = new TreeMap<>();
		} else {
			preferences = init;
		}

		init(Pref.profiling, false);
		init(Pref.profilingToFile, false);
		init(Pref.logNodeID, false);
		for (final InfoType type : InfoType.values()) {
			init(type, true);
		}
	}

	public static Map<String, Object> getPrefs() {
		return Collections.unmodifiableMap(preferences);
	}

	/**
	 * @return true iff we should generate a profile
	 */
	public static boolean getProfiling() {
		return (boolean) get(Pref.profiling);
	}

	/**
	 *
	 * @return true iff generated profile should be saved to a file (instead of
	 *         dumped into the console)
	 */
	public static boolean getProfilingToFile() {
		return (boolean) get(Pref.profilingToFile);
	}

	/**
	 * @return true iff we should log the node IDs
	 */
	public static boolean getLogNodeId() {
		return (boolean) get(Pref.logNodeID);
	}

	/**
	 * @param type the {@link InfoType} that might be selected for display
	 * @return true iff the given {@link InfoType} has been selected (for display)
	 */
	public static boolean isTypeSelected(final InfoType type) {
		return get(type);
	}

	/**
	 * Set the given type to be displayed or not
	 *
	 * @param type    the {@link InfoType}
	 * @param display true to display the given type, false to not display it.
	 */
	public static void setTypeSelected(final InfoType type, final boolean display) {
		put(type, display);
	}

	/**
	 * Set profiling on or off
	 *
	 * @param enable true to enable, false to disable.
	 */
	public static void setProfiling(final boolean enable) {
		put(Pref.profiling, enable);
	}

	/**
	 * Store the profiling results to file or not
	 *
	 * @param enable true to enable, false to disable.
	 */
	public static void setProfilingToFile(final boolean enable) {
		put(Pref.profilingToFile, enable);
	}

	/**
	 * Set logging of node IDs on or off
	 *
	 * @param enable true to enable, false to disable
	 */
	public static void setLogNodeId(final boolean enable) {
		put(Pref.logNodeID, enable);
	}

	private static Object get(final Pref pref) {
		if (preferences == null) {
			Preferences.initializeAllPrefs();
		}
		return preferences.get(pref.name());
	}

	private static Boolean get(final InfoType type) {
		if (preferences == null) {
			Preferences.initializeAllPrefs();
		}
		return (Boolean) preferences.get(type.name());
	}

	private static void put(final Pref pref, final Object value) {
		if (preferences == null) {
			Preferences.initializeAllPrefs();
		}
		preferences.put(pref.name(), value);
	}

	private static void put(final InfoType type, final Boolean value) {
		if (preferences == null) {
			Preferences.initializeAllPrefs();
		}
		preferences.put(type.name(), value);
	}

	private static void init(final Pref pref, final Object defaultValue) {
		final Object current = get(pref);
		if (current == null || !current.getClass().equals(defaultValue.getClass())) {
			put(pref, defaultValue);
		}
	}

	private static void init(final InfoType type, final Boolean defaultValue) {
		final Boolean current = get(type);
		if (current == null || !current.getClass().equals(defaultValue.getClass())) {
			put(type, defaultValue);
		}
	}

	/**
	 * Hide constructor.
	 */
	private ProfilerPreferences() {
	}

}
