package marbel.util.datatable;

/**
 * Prototype column description for Data Table.
 */
public class Column {

	private final String description;

	public Column(final String name) {
		this.description = name;
	}

	public String getDescription() {
		return this.description;
	}

	@Override
	public String toString() {
		return this.description;
	}
}
