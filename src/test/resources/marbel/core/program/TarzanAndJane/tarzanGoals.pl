wantMeeting(date(1, 02, 2010), time(12,00), duration(1,00), [jane, tarzan]).

not_yet_invited(Person, Date, Time, Duration, Attendees) :-
	wantMeeting(Date, Time, Duration, Attendees), member(Person, Attendees), Person \= tarzan,
	not(sentInvitation(Person, meeting(Date, Time, Duration, Attendees))).

meeting_invitation_accepted_by_all(Date, Time, Duration, Attendees) :-
	wantMeeting(Date, Time, Duration, Attendees), del(Attendees, tarzan, Others), 
	forall(member(X, Others), receivedAccept(X, meeting(Date, Time, Duration, Attendees))).
