/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.agents;

import org.junit.Test;

import marbel.tools.test.AbstractTest;
import marbel.tools.test.result.TestProgramResult;

/**
 * Various test cases for passing on variable bindings via a module parameter.
 */
public class VarSubstitution extends AbstractTest {
	/**
	 * Tests whether only module parameters are passed into rules of module itself.
	 * Module parameters should act as a filter that prevents any variable bindings
	 * that do not bind a formal parameter of the module to be applied to the rules
	 * of the module.
	 */
	@Test
	public void moduleParameterFilterTest() throws Exception {
		final TestProgramResult results = runTest(
				"src/test/resources/marbel/core/program/moduleParameterFilter/moduleParameterFilter.test2g");
		assertPassedAndPrint(results);
	}

	/**
	 * Tests whether a parameter is passed correctly into a nested rule section.
	 */
	@Test
	public void passParameterToNestedRulesTest() throws Exception {
		final TestProgramResult results = runTest(
				"src/test/resources/marbel/core/program/passParameterToNestedRules/passParameterToNestedRules.test2g");
		assertPassedAndPrint(results);
	}

	/**
	 * Tests whether a module parameter is correctly passed on to rule condition.
	 */
	@Test
	public void passParameterToRuleConditionTest() throws Exception {
		final TestProgramResult results = runTest(
				"src/test/resources/marbel/core/program/passParameterToRuleCondition/passParameterToRuleCondition.test2g");
		assertPassedAndPrint(results);
	}
}