package marbel.tools.logging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.junit.Before;
import org.junit.Test;

public class CsvFormatterTest {

	private final CsvFormatter formatter = new CsvFormatter();

	@Before
	public void before() {
		// set formatter to UTC to ensure conversion timezone
		this.formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	@Test
	@SuppressWarnings("deprecation")
	public void testTimeZero() {
		// zero means GMT: Thursday, January 1, 1970 12:00:00 AM
		final LogRecord record = new LogRecord(Level.WARNING, "");
		record.setMillis(0L);
		assertEquals("00:00:00.000,,\n", this.formatter.format(record));
	}

	@Test
	@SuppressWarnings("deprecation")
	public void testTime() {
		final LogRecord record = new LogRecord(Level.WARNING, "");
		record.setMillis(1500000000);
		// GMT: Friday, July 14, 2017 2:40:00 AM
		assertEquals("08:40:00.000,,\n", this.formatter.format(record));
	}

	@Test
	public void testMessage() {
		final LogRecord record = new LogRecord(Level.WARNING, "hello");
		assertEquals("hello", getFormattedMessage(record));
	}

	@Test
	public void testMessageQuotes() {
		final LogRecord record = new LogRecord(Level.WARNING, "hello \"quotes\"");
		assertEquals("\"hello \"\"quotes\"\"\"", getFormattedMessage(record));
	}

	@Test
	public void testMessageNewline() {
		final LogRecord record = new LogRecord(Level.WARNING, "hello\nbye!");
		assertEquals("\"hello\nbye!\"", getFormattedMessage(record));
	}

	@Test
	public void testTabEscapement() {
		final LogRecord record = new LogRecord(Level.WARNING, "hello\tbye!");
		assertEquals("\"hello\tbye!\"", getFormattedMessage(record));
	}

	@Test
	public void testStacktrace() {
		final LogRecord record = new LogRecord(Level.WARNING, "");
		try {
			throw new IllegalArgumentException("test!");
		} catch (final Exception e) {
			record.setThrown(e);
		}
		final String exceptionstring = getException(record);
		final String[] lines = exceptionstring.split("\\n");
		assertTrue(lines.length > 20);
		final String expected = "LogRecord test! \"marbel.tools.logging.CsvFormatterTest.testStacktrace(CsvFormatterTest.java:";
		assertEquals(expected, lines[0].substring(0, expected.length()));
	}

	private String getFormattedMessage(final LogRecord record) {
		return this.formatter.format(record).split(",")[1];
	}

	private String getException(final LogRecord record) {
		return this.formatter.format(record).trim().split(",")[2];
	}

}
