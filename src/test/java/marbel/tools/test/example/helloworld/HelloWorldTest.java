package marbel.tools.test.example.helloworld;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assume.assumeTrue;

import org.junit.Test;

import marbel.languageTools.program.test.TestProgram;
import marbel.preferences.LoggingPreferences;
import marbel.tools.TestResultInspector;
import marbel.tools.test.AbstractTest;
import marbel.tools.test.example.helloworld.HistoryTest.HistoryTestRun;

public class HelloWorldTest extends AbstractTest {
	@Test
	public void testHelloWorldBase() throws Exception {
		assumeTrue(hasUI());
		// Make sure history logging is (still) disabled
		final boolean previous = LoggingPreferences.getEnableHistory();
		LoggingPreferences.setEnableHistory(false);

		try {
			// Set-up the helloWorld10x test
			final TestProgram testProgram = setup(
					"src/test/resources/marbel/tools/test/example/helloworld/HelloWorldTest.test2g");
			assertNotNull(testProgram);
			final HistoryTestRun testRun = new HistoryTestRun(testProgram);
			testRun.setDebuggerOutput(true);
			final TestResultInspector inspector = new TestResultInspector(testProgram);
			testRun.setResultInspector(inspector);

			// Run the test and verify its results
			try {
				final long start = System.nanoTime();
				testRun.run(true);
				final long diff = System.nanoTime() - start;
				System.out.println("run time: " + (diff / 1000000000.0) + "s");
				assertPassedAndPrint(inspector.getResults());
			} finally {
				testRun.getAgent().dispose(true);
			}
		} finally {
			// Reset the preference to the original setting
			LoggingPreferences.setEnableHistory(previous);
		}
	}
}
