/**

 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.tools.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;

import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.test.TestValidator;
import marbel.languageTools.program.test.TestProgram;
import marbel.preferences.CorePreferences;
import marbel.preferences.DebugPreferences;
import marbel.tools.TestResultInspector;
import marbel.tools.TestRun;
import marbel.tools.debugger.events.DebugChannel;
import marbel.tools.debugger.events.DebugChannel.ChannelState;
import marbel.tools.errorhandling.exceptions.GOALRunFailedException;
import marbel.tools.logging.Loggers;
import marbel.tools.test.result.TestProgramResult;
import marbel.tools.test.result.TestResultFormatter;

public class AbstractTest {
	private static boolean previous;
	protected FileRegistry registry;
	protected TestValidator visitor;

	private final static Object[][] debugPrefs = { { DebugChannel.ACTIONCOMBO_END, ChannelState.HIDDEN },
			{ DebugChannel.ACTIONCOMBO_START, ChannelState.HIDDEN }, { DebugChannel.ACTION_END, ChannelState.HIDDEN },
			{ DebugChannel.ACTION_EXECUTED_BUILTIN, ChannelState.NONE },
			{ DebugChannel.ACTION_EXECUTED_MESSAGING, ChannelState.NONE },
			{ DebugChannel.ACTION_EXECUTED_USERSPEC, ChannelState.VIEW },
			{ DebugChannel.ACTION_START, ChannelState.HIDDEN }, { DebugChannel.KR_UPDATES, ChannelState.VIEW },
			{ DebugChannel.BREAKPOINTS, ChannelState.HIDDENPAUSE },
			{ DebugChannel.CALL_ACTION_OR_MODULE, ChannelState.PAUSE },
			{ DebugChannel.CLEARSTATE, ChannelState.HIDDEN }, { DebugChannel.DB_QUERY_END, ChannelState.HIDDEN },
			{ DebugChannel.DB_QUERY_START, ChannelState.HIDDEN }, { DebugChannel.DELETE_END, ChannelState.HIDDEN },
			{ DebugChannel.DELETE_START, ChannelState.HIDDEN },
			{ DebugChannel.HIDDEN_RULE_CONDITION_EVALUATION, ChannelState.HIDDEN },
			{ DebugChannel.INSERT_END, ChannelState.HIDDEN }, { DebugChannel.INSERT_START, ChannelState.HIDDEN },
			{ DebugChannel.MAILS, ChannelState.NONE }, { DebugChannel.MODULE_ENTRY, ChannelState.PAUSE },
			{ DebugChannel.MODULE_EXIT, ChannelState.NONE }, { DebugChannel.MSQUERY_END, ChannelState.HIDDEN },
			{ DebugChannel.MSQUERY_START, ChannelState.HIDDEN }, { DebugChannel.NONE, ChannelState.NONE },
			{ DebugChannel.PERCEPTS, ChannelState.NONE }, { DebugChannel.PRINT, ChannelState.HIDDENVIEW },
			{ DebugChannel.REASONING_CYCLE_SEPARATOR, ChannelState.VIEW },
			{ DebugChannel.RULE_CONDITIONAL_VIEW, ChannelState.CONDITIONALVIEW },
			{ DebugChannel.RULE_CONDITION_EVALUATION, ChannelState.PAUSE },
			{ DebugChannel.RULE_EVAL_CONDITION_DONE, ChannelState.HIDDEN },
			{ DebugChannel.RULE_EXIT, ChannelState.HIDDEN }, { DebugChannel.RULE_START, ChannelState.HIDDEN },
			{ DebugChannel.RUNMODE, ChannelState.HIDDEN }, { DebugChannel.SLEEP, ChannelState.VIEW },
			{ DebugChannel.TESTFAILURE, ChannelState.VIEWPAUSE }, { DebugChannel.WARNING, ChannelState.NONE } };

	@Before
	public void start() {
		DebugPreferences.setDefault(getDefaultPrefs());
		Loggers.addConsoleLogger();
		previous = CorePreferences.getAbortOnTestFailure();
		CorePreferences.setAbortOnTestFailure(true);
	}

	@After
	public void end() {
		CorePreferences.setAbortOnTestFailure(previous);
		Loggers.removeConsoleLogger();
	}

	protected static void assertPassedAndPrint(final TestProgramResult results) {
		final TestResultFormatter formatter = new TestResultFormatter();
		System.out.println(formatter.visit(results));
		assertTrue(results.isPassed());
	}

	protected static void assertFailedAndPrint(final TestProgramResult results) {
		final TestResultFormatter formatter = new TestResultFormatter();
		System.out.println(formatter.visit(results));
		assertFalse(results.isPassed());
	}

	public TestProgram setup(final String path) throws Exception {
		this.registry = new FileRegistry();
		this.visitor = new TestValidator(path, this.registry);
		this.visitor.validate();
		final TestProgram program = this.visitor.getProgram();
		if (program == null || this.registry.hasAnyError()) {
			throw new Exception(this.registry.getAllErrors().toString());
		} else {
			return program;
		}
	}

	protected TestProgramResult runTest(final String testFileName) throws Exception {
		TestProgram testProgram;
		try {
			testProgram = setup(testFileName);
		} catch (final IOException e) {
			throw new GOALRunFailedException("error while reading test file " + testFileName, e);
		}

		assertNotNull(testProgram);

		final TestRun testRun = new TestRun(testProgram, false);
		testRun.setDebuggerOutput(true);
		final TestResultInspector inspector = new TestResultInspector(testProgram);
		testRun.setResultInspector(inspector);
		testRun.run(true);

		return inspector.getResults();
	}

	protected static boolean hasUI() {
		try {
			return (GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices().length > 0);
		} catch (final Exception any) {
			return false;
		}
	}

	protected static boolean isJava16or17() {
		return System.getProperty("java.version").startsWith("16")
				|| System.getProperty("java.version").startsWith("17");
	}

	private Map<String, Object> getDefaultPrefs() {
		final Map<String, Object> map = new HashMap<>();
		for (final Object[] keyvalue : debugPrefs) {
			map.put(keyvalue[0].toString(), keyvalue[1].toString());
		}
		return map;
	}
}