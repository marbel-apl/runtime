/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.core.runtime.service.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import marbel.core.agent.AbstractAgentFactory;
import marbel.core.agent.AgentFactory;
import marbel.core.agent.GOALInterpreter;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.mas.MASValidator;
import marbel.languageTools.program.mas.MASProgram;
import marbel.preferences.DebugPreferences;
import marbel.tools.Run;
import marbel.tools.debugger.Debugger;
import marbel.tools.debugger.NOPDebugger;
import marbel.tools.profiler.Profiles;

public class AgentServiceTest {
	private AgentService<Debugger, GOALInterpreter<Debugger>> runtimeService;

	@Before
	public void setUp() throws Exception {
		DebugPreferences.setDefault(Run.getDefaultPrefs());
		final String filename = "src/test/resources/marbel/agents/fibonaci4x.mas2g";
		final FileRegistry registry = new FileRegistry();
		final MASValidator mas2g = new MASValidator(filename, registry);
		mas2g.validate();
		final MASProgram program = mas2g.getProgram();
		mas2g.process();
		if (registry.hasAnyError()) {
			throw new Exception(registry.getAllErrors().toString());
		}

		final AgentFactory<Debugger, GOALInterpreter<Debugger>> factory = new AbstractAgentFactory<Debugger, GOALInterpreter<Debugger>>(
				0) {
			@Override
			protected Debugger provideDebugger() {
				return new NOPDebugger(getAgentId());
			}

			@Override
			protected GOALInterpreter<Debugger> provideController(final Debugger debugger, final Profiles profiles) {
				return new GOALInterpreter<>(getAgentDf(), getRegistry(), debugger, profiles);
			}
		};

		this.runtimeService = new AgentService<>(program, factory);
	}

	@After
	public void tearDown() throws Exception {
		this.runtimeService.awaitTermination();
		this.runtimeService.dispose();
	}

	private int agentsStarted = 0;

	@Test
	public void testStartStop() throws Exception {
		this.runtimeService.addObserver((rs, evt) -> AgentServiceTest.this.agentsStarted++);

		this.runtimeService.start();
		this.runtimeService.shutDown();
		this.runtimeService.awaitTermination();

		assertEquals(4, this.agentsStarted);
		assertEquals(4, this.runtimeService.getAgents().size());
		assertTrue(this.runtimeService.getAliveAgents().isEmpty());
	}
}