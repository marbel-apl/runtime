package marbel.core.executors.modules;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import marbel.core.agent.Controller;
import marbel.core.executors.stack.CallStack;
import marbel.core.executors.stack.RuleStackExecutor;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.AgentActionResult.RunStatus;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.ExitCondition;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.agent.rules.Rule;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

public class LinearModuleExecutorTest {
	/**
	 * Default test. We pop the next executor but the module has no rules so the
	 * module should exit soft.
	 */
	@Test
	public void testExitModule() throws GOALActionFailedException {

		final CallStack stack = mock(CallStack.class);
		final RunState runstate = mock(RunState.class);
		final ExecutionEventGeneratorInterface eventgenerator = mock(ExecutionEventGeneratorInterface.class);
		when(runstate.getEventGenerator()).thenReturn(eventgenerator);
		final Module module = mock(Module.class);
		final Substitution subst = mock(Substitution.class);
		final LinearModuleExecutor executor = new LinearModuleExecutor(stack, runstate, module, subst,
				RuleEvaluationOrder.LINEARALL);

		// this mock module has no rules so it will terminate when popped
		executor.popped();
		assertEquals(RunStatus.SOFT_TERMINATED, executor.getResult().getStatus());
	}

	/**
	 * Hard exit test. We pop the next executor but the parent indicates that the
	 * previous call exited hard. The module should now exit hard too (even though
	 * we're at the end of the module anyway which would trigger a soft exit).
	 */
	@Test
	public void testExitModuleHard() throws GOALActionFailedException {
		final Controller parentController = mock(Controller.class);

		// in the parent, the rule executor terminated hard. This means the
		// previous action terminated hard.
		final RuleStackExecutor parentexecutor = mock(RuleStackExecutor.class);
		when(parentexecutor.getResult()).thenReturn(new AgentActionResult(false, false, RunStatus.HARD_TERMINATED));

		final CallStack stack = mock(CallStack.class);
		when(stack.getPopped()).thenReturn(parentexecutor);

		final RunState runstate = mock(RunState.class);
		when(runstate.getParent()).thenReturn(parentController);

		final ExecutionEventGeneratorInterface eventgenerator = mock(ExecutionEventGeneratorInterface.class);
		when(runstate.getEventGenerator()).thenReturn(eventgenerator);

		final Module module = mock(Module.class);
		when(module.getExitCondition()).thenReturn(ExitCondition.NEVER);
		final Substitution subst = mock(Substitution.class);
		final LinearModuleExecutor executor = new LinearModuleExecutor(stack, runstate, module, subst,
				RuleEvaluationOrder.LINEARALL);
		// this mock module has no rules so it will terminate when popped
		executor.popped();
		assertEquals(RunStatus.HARD_TERMINATED, executor.getResult().getStatus());
	}

	/**
	 * Hard exit test 2. We pop the next executor but the parent indicates that the
	 * previous call exited hard. The module should now exit hard even though there
	 * are more rules availale in the module
	 */
	@Test
	public void testExitModuleHardMoreRules() throws GOALActionFailedException {
		final Controller parentController = mock(Controller.class);

		// in the parent, the rule executor terminated hard. This means the
		// previous action terminated hard.
		final RuleStackExecutor parentexecutor = mock(RuleStackExecutor.class);
		when(parentexecutor.getResult()).thenReturn(new AgentActionResult(false, false, RunStatus.HARD_TERMINATED));

		final CallStack stack = mock(CallStack.class);
		when(stack.getPopped()).thenReturn(parentexecutor);

		final RunState runstate = mock(RunState.class);
		when(runstate.getParent()).thenReturn(parentController);

		final ExecutionEventGeneratorInterface eventgenerator = mock(ExecutionEventGeneratorInterface.class);
		when(runstate.getEventGenerator()).thenReturn(eventgenerator);

		final Module module = mock(Module.class);
		final List<Rule> rules = new ArrayList<>();
		final Rule rule1 = mock(Rule.class);
		rules.add(rule1);
		when(module.getExitCondition()).thenReturn(ExitCondition.NEVER);
		when(module.getRules()).thenReturn(rules);

		final Substitution subst = mock(Substitution.class);
		final LinearModuleExecutor executor = new LinearModuleExecutor(stack, runstate, module, subst,
				RuleEvaluationOrder.LINEARALL);
		// this mock module has no rules so it will terminate when popped
		executor.popped();
		assertEquals(RunStatus.HARD_TERMINATED, executor.getResult().getStatus());
	}

}
