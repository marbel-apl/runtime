package marbel.core.executors.stack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;

/**
 * Stubbing class for Substitution.
 *
 * <h1>Explanation</h1> It would be better if we could use a mock instead of
 * this stub. However, mocking Substitution poses problems, because of the
 * functions that can modify Substitution itself. For example, suppose we have a
 * mock M for the empty substitution. Now someone calls M.add([X/3]). We can not
 * return a new Substitution mocking [X/3] because add is void.
 *
 * @author W.Pasman 24sep15
 */
public class SubstitutionStub implements Substitution {
	@Override
	public int hashCode() {
		return this.map.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof SubstitutionStub)) {
			return false;
		}
		final SubstitutionStub other = (SubstitutionStub) obj;
		if (!Objects.equals(this.map, other.map)) {
			return false;
		}
		return true;
	}

	private final Map<Var, Term> map;

	/**
	 * Empty substi
	 */
	public SubstitutionStub() {
		this.map = new LinkedHashMap<>();
	}

	/**
	 * Substitution with one element
	 */
	public SubstitutionStub(final Var v, final Term t) {
		this();
		addBinding(v, t);
	}

	/**
	 * Constructor that takes a complete map of var/term values.
	 *
	 * @param m
	 */
	public SubstitutionStub(final Map<Var, Term> m) {
		this.map = m;
	}

	@Override
	public List<Var> getVariables() {
		return new ArrayList<>(this.map.keySet());
	}

	@Override
	public Term get(final Var var) {
		return this.map.get(var);
	}

	@Override
	public void addBinding(final Var var, final Term term) {
		if (!this.map.containsKey(var)) {
			this.map.put(var, term);
		}
	}

	@Override
	public Substitution combine(final Substitution substitution) {
		for (final Var var : substitution.getVariables()) {
			if (this.map.containsKey(var)) {
				return null;
			}
		}
		final Substitution newsubst = substitution.clone();
		for (final Var var : this.map.keySet()) {
			newsubst.addBinding(var, this.map.get(var));
		}
		return newsubst;
	}

	@Override
	public boolean remove(final Var var) {
		return (this.map.remove(var) != null);
	}

	@Override
	public boolean retainAll(final Collection<Var> variables) {
		boolean removedSomething = false;
		final Set<Var> check = this.map.keySet();
		for (final Var var : check) {
			if (!variables.contains(var)) {
				removedSomething |= remove(var);
			}
		}
		return removedSomething;
	}

	@Override
	public Substitution clone() {
		return new SubstitutionStub(new LinkedHashMap<>(this.map));
	}

	@Override
	public String toString() {
		return this.map.toString();
	}
}
