package marbel.core.executors.stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import marbel.core.runtime.mentalstate.MentalStateConditionExecutor;
import marbel.core.runtime.mentalstate.MentalStateConditionResult;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.KRInterface;
import marbel.krInterface.exceptions.KRQueryFailedException;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.rules.IfThenRule;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

/**
 * Unit tests for the {@link RuleStackExecutor}. We mock/stub a lot of classes
 * because executors are triggering a lot of machinery. We also try to mock
 * terms a bit deeper so that we can actually see what is going on if something
 * fails (both for better error messages and to support debugging).
 */
public class RuleStackExecutorTest {
	private RunState runstate;
	private Module context;
	private ExecutionEventGeneratorInterface debugger;
	private MentalStateConditionExecutor mockedMentalStateConditionExecutor;
	private final Substitution emptySubst = new SubstitutionStub();
	private final KRInterface krInterface = mock(KRInterface.class);

	@Before
	public void setup() throws Exception {
		this.debugger = mock(ExecutionEventGeneratorInterface.class);

		this.context = mock(Module.class);
		when(this.context.getKRInterface()).thenReturn(this.krInterface);
		when(this.krInterface.getSubstitution(null)).thenReturn(this.emptySubst);

		final MentalStateWithEvents mentalstate = mock(MentalStateWithEvents.class);
		this.mockedMentalStateConditionExecutor = mock(MentalStateConditionExecutor.class);
		when(mentalstate.getConditionExecutor(any(), any())).thenReturn(this.mockedMentalStateConditionExecutor);

		this.runstate = mock(RunState.class);
		when(this.runstate.getEventGenerator()).thenReturn(this.debugger);
		when(this.runstate.getActiveModule()).thenReturn(this.context);
		when(this.runstate.getMentalState()).thenReturn(mentalstate);
		when(this.runstate.getKRI()).thenReturn(this.krInterface);
	}

	/**
	 * test the call of if bel(false) then insert(q).
	 */
	@Test
	public void doesNotHold() throws Exception {
		final CallStack parent_callstack = mock(CallStack.class);
		when(this.context.getRuleEvaluationOrder()).thenReturn(RuleEvaluationOrder.LINEAR);

		final MentalLiteral belFalse = mockMentalStateCondition("bel(false)");
		final ActionCombo insertQ = mockActionCombo("insert(q)");
		final IfThenRule rule = mockIfThenRule(belFalse, insertQ);
		// result object containing no solutions.
		final List<Substitution> answers = new ArrayList<>(0);
		// mock the MSCResult which will be the result of a mentalstate call.
		final MentalStateConditionResult substResult = mockMSCResult(answers);
		when(this.mockedMentalStateConditionExecutor.evaluate(any(), any())).thenReturn(substResult);

		/**
		 * Finally, create the object to be tested. No override of factories (
		 * getExecutor), we will check that it's not used
		 */
		final RuleStackExecutor executorReal = new RuleStackExecutor(parent_callstack, this.runstate, rule,
				this.emptySubst);

		// we spy calls to the StackExecutor factory to check the created
		// objects.
		final RuleStackExecutor executor = spy(executorReal);

		executor.setContext(this.context);

		// TEST pushed.
		// executor.pushed();
		// verifyRunstateUnchanged();
		// checkBreakpointsReportedFail();
		// verify(this.runstate, never()).doLog(any());
		// assertEquals(this.context, this.runstate.getActiveModule());

		// TEST popped.
		executor.popped();
		verifyRunstateUnchanged();
		verify(executor, never()).getExecutor(any(), any());
		verify(parent_callstack, never()).push(any());
	}

	/**
	 * Test where the MS query throws an exception. This should result in a failure
	 * occuring in the pushed() call, which should be remembered and returned with
	 * getResult().
	 */
	@Test(expected = GOALActionFailedException.class)
	public void throwingMSQ() throws Exception {
		final CallStack parent_callstack = mock(CallStack.class);
		when(this.context.getRuleEvaluationOrder()).thenReturn(RuleEvaluationOrder.LINEAR);

		final MentalLiteral badMSQ = mockMentalStateCondition("**badquery**");
		final ActionCombo insertQ = mockActionCombo("insert(q)");
		final IfThenRule rule = mockIfThenRule(badMSQ, insertQ);
		when(this.mockedMentalStateConditionExecutor.evaluate(any(), any()))
				.thenThrow(new KRQueryFailedException("failed"));

		/**
		 * Finally, create the object to be tested. No override of factories (
		 * getExecutor), we will check that it's not used
		 */
		final RuleStackExecutor executor = new RuleStackExecutor(parent_callstack, this.runstate, rule,
				this.emptySubst);

		executor.setContext(this.context);

		executor.popped();
		executor.getResult();
	}

	@Test
	public void testRandom() throws Exception {
		testRandomActionChoices(RuleEvaluationOrder.RANDOM);
	}

	@Test
	public void testLinearRandom() throws Exception {
		testRandomActionChoices(RuleEvaluationOrder.LINEARRANDOM);
	}

	@Test
	public void testRandomAll() throws Exception {
		testRandomActionChoices(RuleEvaluationOrder.RANDOMALL);
	}

	// @Test
	// public void testAdaptive() throws Exception,
	// MSTDatabaseException, MSTQueryException {
	// testFirstActionChosen(RuleEvaluationOrder.ADAPTIVE);
	// }

	@Test
	public void testLinear() throws Exception {
		testFirstActionChosen(RuleEvaluationOrder.LINEAR);
	}

	// @Test
	// public void testLinearAdaptive() throws Exception {
	// testFirstActionChosen(RuleEvaluationOrder.LINEARADAPTIVE);
	// }

	@Test
	public void testLinearAll() throws Exception {
		testFirstActionChosen(RuleEvaluationOrder.LINEARALL);
	}

	/********************* MOCK SUPPORT FUNCTIONS **************************/
	/**
	 * @param mscName
	 * @param freeVarNames the free variables in the {@link MentalStateCondition}
	 * @return Mocked {@link MentalStateCondition} containing just a string as term.
	 */
	private MentalLiteral mockMentalStateCondition(final String mscName, final Var... freeVars) {
		final MentalLiteral msc = mock(MentalLiteral.class);
		when(msc.toString()).thenReturn(mscName);
		final List<Var> vars = List.of(freeVars);
		when(msc.getFreeVar()).thenReturn(vars);

		return msc;
	}

	/**
	 * *
	 *
	 * @param termName String holding the term.
	 * @return Mock of the term.
	 */
	private Term mockTerm(final String termName) {
		/*
		 * We need to mock terms because {@link RuleStackExecutor#substitutionsToTerm}
		 * manipulates on that level...
		 */
		final Term term = mock(Term.class);
		when(term.toString()).thenReturn(termName);
		return term;
	}

	/**
	 * Mock a rule. The rule does not react to substitution attempts.
	 *
	 * @param cond
	 * @param actions
	 * @param focus
	 * @return
	 */
	private IfThenRule mockIfThenRule(final MentalLiteral cond, final ActionCombo actions) {
		final IfThenRule rule = mock(IfThenRule.class);
		// if marbel(objective(OBJ) then objectiveHandler.
		when(rule.getCondition()).thenReturn(cond);
		when(rule.getAction()).thenReturn(actions);
		when(rule.applySubst(any())).thenReturn(rule);

		// we have to create the string first, because thenReturn will not
		// accept mock objects.
		final String lookslike = "if " + cond.toString() + " then " + actions.toString();
		when(rule.toString()).thenReturn(lookslike);
		return rule;
	}

	/**
	 *
	 * @param comboName
	 * @return Mocked action combo with given name
	 */
	private ActionCombo mockActionCombo(final String comboName) {
		final ActionCombo combo = mock(ActionCombo.class);
		when(combo.toString()).thenReturn(comboName);
		return combo;
	}

	/**
	 *
	 * @param string name of var
	 * @return mocked {@link Var}
	 */
	private Var mockVar(final String string) {
		final Var var = mock(Var.class);
		when(var.toString()).thenReturn(string);
		return var;
	}

	/**
	 *
	 *
	 * @param answers
	 * @param focusedGoals
	 * @return Mocked {@link MentalStateConditionResult}. Assumes the result is not
	 *         empty and that this is also the result for getFocusedResults.
	 */
	private MentalStateConditionResult mockMSCResult(final List<Substitution> answers) {
		final MentalStateConditionResult substResult = mock(MentalStateConditionResult.class);
		when(substResult.getAnswers()).thenReturn(answers);
		when(substResult.holds()).thenReturn(!answers.isEmpty());

		final String pretty = "MSCResult(" + answers + ")";
		when(substResult.toString()).thenReturn(pretty);
		return substResult;
	}

	/**
	 * verify that the run state was not modified
	 */
	private void verifyRunstateUnchanged() throws Exception {
		verify(this.runstate, never()).doPerformAction(any());
		verify(this.runstate, never()).enterModule(any());
		verify(this.runstate, never()).exitModule(any());
		verify(this.runstate, never()).incrementRoundCounter();
		verify(this.runstate, never()).reset();
		verify(this.runstate, never()).startCycle(anyBoolean());
		verify(this.runstate, never()).dispose();
	}

	/**
	 * We execute a rule if p(Y) then insert(q(Y)). The module is using the given
	 * {@link RuleEvaluationOrder}. Test that a random applicable instantiation is
	 * applied (even distribution of the possible actions 1,3 and 7)
	 *
	 * @param randomorder the {@link RuleEvaluationOrder}. Must be some order that
	 *                    randomizes the rule evaluation.
	 *
	 */
	private void testRandomActionChoices(final RuleEvaluationOrder randomorder) throws Exception {
		final List<String> actions = runMany(randomorder);

		checkFlatDistribution(actions, "1", "3", "7");
	}

	/**
	 * Test that always the first of the possible action choices is chosen.
	 *
	 * @param nonrandomorder the {@link RuleEvaluationOrder}. Must be some order
	 *                       that does not randomize the rule evaluation.
	 *
	 */
	private void testFirstActionChosen(final RuleEvaluationOrder nonrandomorder) throws Exception {
		for (final String action : runMany(nonrandomorder)) {
			assertEquals("1", action);
		}
	}

	/**
	 * Execute a rule if p(Y) then insert(q(Y)) many times. The beliefs are p(1),
	 * p(3), p(7).
	 *
	 * @param randomorder the {@link RuleEvaluationOrder}.
	 *
	 * @return a list with elements "1","3" and "7", where "1" indicates that
	 *         insert(q(1)) was selected, "3" that insert(q(3)) was selected, etc.
	 * @throws MSTQueryException
	 * @throws MSTDatabaseException
	 */
	private List<String> runMany(final RuleEvaluationOrder randomorder) throws Exception {
		final int LARGE = 100;

		final CallStackStub parent_callstack = new CallStackStub();

		when(this.context.getRuleEvaluationOrder()).thenReturn(randomorder);

		final Var X = mockVar("X");
		final Term t1 = mockTerm("1");
		final Term t3 = mockTerm("3");
		final Term t7 = mockTerm("7");
		final Substitution subst_X_1 = new SubstitutionStub(X, t1);
		final Substitution subst_X_3 = new SubstitutionStub(X, t3);
		final Substitution subst_X_7 = new SubstitutionStub(X, t7);

		final List<Term> set137 = new ArrayList<>();
		set137.add(t1);
		set137.add(t3);
		set137.add(t7);

		final MentalLiteral bel_p_X = mockMentalStateCondition("bel(p(X))", X);
		final ActionCombo insert_q_X = mockActionCombo("insert(q(X))");
		final IfThenRule rule = mockIfThenRule(bel_p_X, insert_q_X);

		// [OBJ/a] substitution. MUST BE LinkedHashSet to retain insertion
		// order.
		final List<Substitution> answers = new ArrayList<>(3);
		answers.add(subst_X_1);
		answers.add(subst_X_3);
		answers.add(subst_X_7);

		// mock the MSCResult which will be the result of a mentalstate call.
		// in this case, the answers are stored under the key 'null' in the
		// MSCResult. (This is not documented in MSCResult)
		final MentalStateConditionResult substResult = mockMSCResult(answers);

		when(this.mockedMentalStateConditionExecutor.evaluate(any(), any())).thenReturn(substResult);

		/**
		 * Execute it a large number of times.
		 */
		for (int n = 0; n < LARGE; n++) {
			/**
			 * Finally, create the object to be tested. Stub the factories.
			 */
			final RuleStackExecutor executor = new RuleStackExecutor(parent_callstack, this.runstate, rule,
					this.emptySubst) {
				/**
				 * We let the mock object toString return the actual insert value. So if the
				 * action is insert(q(X)) we just return the X. This allows we can see the
				 * picked action and easily check if it's random between 1, 3 and 7.
				 */
				@Override
				protected StackExecutor getExecutor(final Object object, final Substitution substitution) {
					final ActionComboStackExecutor mockedActionComboExecutor = mock(ActionComboStackExecutor.class);
					final String string = substitution.get(X).toString();
					when(mockedActionComboExecutor.toString()).thenReturn(string);
					return mockedActionComboExecutor;
				}
			};

			executor.setContext(this.context);
			executor.popped();
		}

		/**
		 * Count number of occurences of the actions.
		 */
		final List<String> actions = new ArrayList<>();
		for (final StackExecutor ex : parent_callstack.getStack()) {
			if (ex instanceof ActionComboStackExecutor) {
				actions.add(((ActionComboStackExecutor) ex).toString());
			}
		}
		return actions;
	}

	/**
	 * Check that a given list is approximately flat distributed. Possible values
	 * must occur approximately equal number of times. This test has a probability
	 * of about 0.001 of failing.
	 *
	 * @param actions a list of simple string objects. Each one must compare equal
	 *                to one of the given values. Must be large, at least 100.
	 * @param values  possible values in the list of actions. must contain at least
	 *                2 values.
	 * @throws IllegalArgumentException if <2 values are provided.
	 */
	private void checkFlatDistribution(final List<String> actions, final String... values) {
		if (values.length < 2) {
			throw new IllegalArgumentException("at least 2 values are required but got only " + values.length);
		}
		/**
		 * Mathematics: We are going to check that for each value v, the number of
		 * occurences O(v) of v in actions is at least some minimum value M.
		 *
		 * <p>
		 * Consider a single value v. Under flat distribution assumptions, the chance of
		 * each action to be of value v is p= 1/|values|.
		 * <p>
		 * We want to be very sure that our tests succeeds, so we have to make M small
		 * enough such P(O(v)<M) very small. We are doing the test for all values, and
		 * they ALL have to succeed. We therefore can aim at P(O(v)<M) < 0.001/|values|.
		 * <p>
		 * But to keep things simple we assume |values| is small, say at most 10, and we
		 * just aim for P(O(v)<M) < 0.001.
		 * <p>
		 * We assume the size of actions N is large. Then we can assume a Normal
		 * Probability distribution for O(v). expectation value E(O(v)) = N p. Var(O(v))
		 * = N p (1-p). Then with Sigma = Sqrt(Var), P(O(v)<M) < 0.001 if M< E - 3.1
		 * Sigma.
		 * <p>
		 * Filling in, M = N p - 3.1 Sqrt[N p (1-p)]. For large N (N>100) this function
		 * behaves roughly as M=(N/2) p. This approximation for M is too small for large
		 * N, so we are on the safe side.
		 */
		final double minimum = 0.5 * (actions.size() / values.length);

		for (final String value : values) {
			assertTrue(Collections.frequency(actions, value) > minimum);
		}

	}

	/**
	 * hack the CallStack for checking. Just collects all pushed data. Would have
	 * been nicer if CallStack would be an interface.
	 */
	private static class CallStackStub extends CallStack {
		List<StackExecutor> executors = new ArrayList<>();

		@Override
		public boolean canExecute() {
			return false;
		}

		@Override
		public int getIndex() {
			return 0;

		}

		@Override
		public void push(final StackExecutor executor) {
			this.executors.add(executor);
		}

		@Override
		public void pop() {
		}

		@Override
		public StackExecutor getPopped() {
			return null;
		}

		public List<StackExecutor> getStack() {
			return this.executors;
		}
	}
}
