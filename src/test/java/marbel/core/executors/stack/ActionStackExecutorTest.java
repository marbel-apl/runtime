package marbel.core.executors.stack;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import marbel.core.executors.modules.ModuleExecutor;
import marbel.core.runtime.mentalstate.MentalStateWithEvents;
import marbel.core.runtime.service.agent.AgentActionResult;
import marbel.core.runtime.service.agent.AgentActionResult.RunStatus;
import marbel.core.runtime.service.agent.RunState;
import marbel.krInterface.language.Substitution;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.tools.debugger.events.ExecutionEventGeneratorInterface;
import marbel.tools.errorhandling.exceptions.GOALActionFailedException;

@RunWith(Parameterized.class)
public class ActionStackExecutorTest {
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { new AgentActionResult(false, false, RunStatus.HARD_TERMINATED) },
				{ new AgentActionResult(true, false, RunStatus.RUNNING) },
				{ new AgentActionResult(true, false, RunStatus.SOFT_TERMINATED) },
				{ new AgentActionResult(false, false, RunStatus.SOFT_TERMINATED) } });
	}

	/**
	 * result from the module that we just called (we simulate this result by
	 * mocking getResult)
	 */
	@Parameter
	public AgentActionResult moduleResult;
	private Module completedModule;
	private ActionStackExecutor executor;

	@Before
	public void before() throws GOALActionFailedException {
		final CallStack stack = mock(CallStack.class);
		final ModuleExecutor parentexecutor = mock(ModuleExecutor.class);
		when(parentexecutor.getResult()).thenReturn(this.moduleResult);
		when(stack.getPopped()).thenReturn(parentexecutor);

		final RunState runstate = mock(RunState.class);
		final ExecutionEventGeneratorInterface eventgenerator = mock(ExecutionEventGeneratorInterface.class);
		when(runstate.getEventGenerator()).thenReturn(eventgenerator);
		final MentalStateWithEvents mentalstate = mock(MentalStateWithEvents.class);
		when(runstate.getMentalState()).thenReturn(mentalstate);

		final ModuleCallAction modulecallaction = mock(ModuleCallAction.class);
		this.completedModule = mock(Module.class);
		when(modulecallaction.getTarget()).thenReturn(this.completedModule);

		final Substitution substitution = mock(Substitution.class);
		this.executor = new ActionStackExecutor(stack, runstate, modulecallaction, substitution, false);
		this.executor.result = AgentActionResult.START;
	}

	/**
	 * Test the result from the module call is derived from the
	 * {@link #moduleResult} if the module is anonymous.
	 *
	 * @throws GOALActionFailedException
	 */
	@Test
	public void testPoppedAnonymousModuleCall() throws GOALActionFailedException {
		when(this.completedModule.isAnonymous()).thenReturn(true);

		this.executor.popped();
		assertEquals(this.moduleResult, this.executor.getResult());
	}

	/**
	 * Test the result from the module call is derived from the
	 * {@link #moduleResult}. Here the module is normal, not anonymous
	 *
	 * @throws GOALActionFailedException
	 */
	@Test
	public void testPoppedModuleCall() throws GOALActionFailedException {
		when(this.completedModule.isAnonymous()).thenReturn(false);

		this.executor.popped();

		// With normal modules, the runmode is not affected by the result from
		// the submodule but the action status is.
		final AgentActionResult expectedResult = new AgentActionResult(this.moduleResult.justPerformedAction(),
				this.moduleResult.justPerformedRealAction(), RunStatus.RUNNING);
		assertEquals(expectedResult, this.executor.getResult());
	}
}
